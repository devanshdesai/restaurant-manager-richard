﻿namespace GlodCleaningClient.Pages
{
    public class Controllers
    {
        public const string Users = "Users";
        public const string Role = "Role";
        public const string Dashboard = "Dashboard";
        public const string Account = "Account";

        public const string Home = "Home";
        public const string HomeCleaning = "HomeCleaning";
        public const string EndOfLeaseCleaning = "EndOfLeaseCleaning";
        public const string OfficeCleaning = "OfficeCleaning";
        public const string BookNow = "BookNow";
        public const string BecomeCleaner = "BecomeCleaner";
        public const string Contact = "Contact";
        public const string AboutUs = "AboutUs";
        public const string Jobs = "Jobs";
        public const string Donors = "Donors";
        public const string LatestAnnouncement = "LatestAnnouncement";
        public const string LatestEvent = "LatestEvent";
        public const string RitRivaz = "RitRivaz";
        public const string Wife = "Wife";
        public const string MainMember = "MainMember";
        public const string CommitteeMember = "CommitteeMember";
        public const string PhotoGallery = "PhotoGallery";
        public const string Daughter = "Daughter";
        public const string SonChild = "SonChild";
        public const string SonWife = "SonWife";
        public const string VideoGallery = "VideoGallery";
        public const string Son = "Son";
        public const string Advertisement = "Advertisement";

    }
}