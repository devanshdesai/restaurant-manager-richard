﻿namespace GlodCleaningClient.Pages
{
    public class Actions
    {
        #region Common
        public const string Index = "Index";
        public const string BindData = "BindData";
        public const string Upsert = "Upsert";
        public const string ActInact = "ActInact";
        public const string ApprovedCleaner = "ApprovedCleaner";
        public const string ApprovedProvider = "ApprovedProvider";
        public const string GetById = "GetById";
        public const string CustomerDetails = "CustomerDetails";
        public const string CleanerDetails = "CleanerDetails";
        public const string ProviderDetails = "ProviderDetails";
        public const string PromotionalCodeDetails = "PromotionalCodeDetails";
        public const string DeletePromotionalCode = "DeletePromotionalCode";
        public const string SupportDetails = "SupportDetails";
        public const string CommitteeMemberDetails = "CommitteeMemberDetails";
        public const string DeleteCommitteeMember = "DeleteCommitteeMember";
        public const string ImageUpsert = "ImageUpsert";
        public const string DonorsDetails = "DonorsDetails";
        public const string DeleteDonors = "DeleteDonors";
        public const string MainMemberDetails = "MainMemberDetails";
        public const string DeleteMainMember = "DeleteMainMember";
        public const string LatestAnnouncementDetails = "LatestAnnouncementDetails";
        public const string DeleteLatestAnnouncement = "DeleteLatestAnnouncement";
        public const string LatestEventDetails = "LatestEventDetails";
        public const string DeleteLatestEvent = "DeleteLatestEvent";
        public const string RitRivazDetails = "RitRivazDetails";
        public const string WifeDetails = "WifeDetails";
        public const string DeleteWife = "DeleteWife";
        public const string PhotoUpload = "PhotoUpload";
        public const string DaughterDetails = "DaughterDetails";
        public const string DeleteDaughter = "DeleteDaughter";
        public const string SonWifeDetails = "SonWifeDetails";
        public const string DeleteSonWife = "DeleteSonWife";
        public const string SonChildDetails = "SonChildDetails";
        public const string DeleteSonChild = "DeleteSonChild";
        public const string VideoGalleryDetails = "VideoGalleryDetails";
        public const string DeleteVideoGallery = "DeleteVideoGallery";
        public const string DeletePhotoGallery = "DeletePhotoGallery";
        public const string DeleteRitRivaz = "DeleteRitRivaz";
        public const string DeleteSon = "DeleteSon";
        public const string SonDetails = "SonDetails";
        public const string BindSonData = "BindSonData";
        public const string BindWifeData = "BindWifeData";
        public const string BindDaughterData = "BindDaughterData";
        public const string BindSonChildData = "BindSonChildData";
        public const string BindSonWifeData = "BindSonWifeData";
        public const string WifeUpsert = "WifeUpsert";
        public const string SonUpsert = "SonUpsert";
        public const string DaughterUpsert = "DaughterUpsert";
        public const string GetBySonId = "GetBySonId";
        public const string AdvanceDirectory = "AdvanceDirectory";
        public const string BindAdvanceDirectoryData = "BindAdvanceDirectoryData";

        #endregion Common   

        #region Home Cleaning
        public const string KitchenCleaning = "KitchenCleaning";
        public const string Bathroom = "Bathroom";
        public const string Bedroom = "Bedroom";
        public const string Extra = "Extra";
        #endregion

        #region Become a Cleaner
        public const string applynow = "applynow";
        #endregion

        #region End Of Lease Cleaning
        public const string Laundry = "Laundry";
        #endregion


        #region Office Cleaning
        public const string Boardrooms = "Boardrooms";
        public const string Desks = "Desks";
        #endregion

        #region Book Now
        public const string HomeCleaning = "HomeCleaning";
        public const string EndOfLeaseCleaning = "EndOfLeaseCleaning";
        public const string OfficeCleaning = "OfficeCleaning";
        #endregion
    }
}