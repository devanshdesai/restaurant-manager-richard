﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class AdvertisementController : Controller
    {
        public readonly AbstractAdvertisementServices abstractAdvertisementServices;

        public AdvertisementController(AbstractAdvertisementServices abstractAdvertisementServices)
        {

            this.abstractAdvertisementServices = abstractAdvertisementServices;
        }
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            var result = abstractAdvertisementServices.Advertisement_All(pageParam, "").Values.ToList();

            return View(result);
        }

    }
}