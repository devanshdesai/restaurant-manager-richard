﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class LatestEventController : Controller
    {
        public readonly AbstractLatestEventServices abstractLatestEventServices;

        public LatestEventController(AbstractLatestEventServices abstractLatestEventServices)
        {

            this.abstractLatestEventServices = abstractLatestEventServices;
        }
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            var result = abstractLatestEventServices.LatestEvent_All(pageParam, "").Values.ToList();

            return View(result);
        }

    }
}