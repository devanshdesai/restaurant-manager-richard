﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using GlodCleaning.Common.Paging;
using GlodCleaningClient.Infrastructure;

namespace GlodCleaningClient.Controllers
{
 
    public class HomeController : BaseController
    {
        public readonly AbstractPageImagesServices abstractPageImagesServices;
        public readonly AbstractLatestAnnouncementServices abstractLatestAnnouncementServices;
        public readonly AbstractPhotoGalleryServices abstractPhotoGalleryServices;
        public readonly AbstractCommitteeMemberServices abstractCommitteeMemberServices;
        public readonly AbstractLatestEventServices abstractLatestEventServices;

        public HomeController(AbstractLatestAnnouncementServices abstractLatestAnnouncementServices, AbstractPhotoGalleryServices abstractPhotoGalleryServices, AbstractCommitteeMemberServices abstractCommitteeMemberServices,AbstractLatestEventServices abstractLatestEventServices, AbstractPageImagesServices abstractPageImagesServices)
        {
            this.abstractPageImagesServices = abstractPageImagesServices;
            this.abstractLatestAnnouncementServices = abstractLatestAnnouncementServices;
            this.abstractPhotoGalleryServices = abstractPhotoGalleryServices;
            this.abstractLatestEventServices = abstractLatestEventServices;
            this.abstractCommitteeMemberServices = abstractCommitteeMemberServices;
        }

        public ActionResult Index()
        {
             HomePage homePage = new HomePage();
            PageParam pageParam = new PageParam();
            homePage.PageImages = abstractPageImagesServices.PageImages_All(pageParam, "", 1).Values.ToList();
            homePage.PageSliderImages = abstractPageImagesServices.PageImages_All(pageParam, "", 5).Values.ToList();
            pageParam.Offset = 0;
            pageParam.Limit = 3;
            homePage.LatestAnnouncement = abstractLatestAnnouncementServices.LatestAnnouncement_All(pageParam, "").Values.ToList();
            homePage.LatestEvent = abstractLatestEventServices.LatestEvent_All(pageParam, "").Values.ToList();
            homePage.CommitteeMember = abstractCommitteeMemberServices.CommitteeMember_All(pageParam, "",0).Values.ToList();
            homePage.PhotoGallery = abstractPhotoGalleryServices.PhotoGallery_All(pageParam, "").Values.ToList();
            return View(homePage);
        }
        
    }
}