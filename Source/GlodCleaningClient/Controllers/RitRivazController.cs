﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.V1;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class RitRivazController : Controller
    {
        public readonly AbstractRitRivazServices abstractRitRivazServices;

        public RitRivazController(AbstractRitRivazServices abstractRitRivazServices)
        {

            this.abstractRitRivazServices = abstractRitRivazServices;
        }
        public ActionResult Index()
        {
            //RitRivaz ritRivaz = new RitRivaz();
            PageParam pageParam = new PageParam();
             var result= abstractRitRivazServices.RitRivaz_All(pageParam, "").Values.ToList();

            return View(result);
        }
        
    }
}