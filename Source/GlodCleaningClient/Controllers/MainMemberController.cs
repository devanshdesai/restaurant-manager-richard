﻿using DataTables.Mvc;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using GlodCleaning.Services.Contract;
using GlodCleaningClient.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class MainMemberController : Controller
    {
        public readonly AbstractMainMemberServices abstractMainMemberServices;
        public readonly AbstractMasterBloodGroupServices abstractMasterBloodGroupServices;
        public readonly AbstractSonServices abstractSonServices;
        public readonly AbstractWifeServices abstractWifeServices;
        public readonly AbstractDaughterServices abstractDaughterServices;
        public readonly AbstractMasterDistrictServices abstractMasterDistrictServices;
        private readonly AbstractSonChildServices abstractSonChildServices;
        private readonly AbstractSonWifeServices abstractSonWifeServices;
        private readonly AbstractMasterCityServices abstractMasterCityServices;
        private readonly AbstractMasterEducationServices abstractMasterEducationServices;

        public MainMemberController(AbstractMainMemberServices abstractMainMemberServices, AbstractMasterCityServices abstractMasterCityServices, AbstractMasterEducationServices abstractMasterEducationServices, AbstractMasterBloodGroupServices abstractMasterBloodGroupServices,AbstractSonServices abstractSonServices,AbstractWifeServices abstractWifeServices, AbstractDaughterServices abstractDaughterServices, AbstractMasterDistrictServices abstractMasterDistrictServices, AbstractSonWifeServices abstractSonWifeServices, AbstractSonChildServices abstractSonChildServices)
        {

            this.abstractMainMemberServices = abstractMainMemberServices;
            this.abstractSonServices = abstractSonServices;
            this.abstractMasterBloodGroupServices = abstractMasterBloodGroupServices;
            this.abstractWifeServices = abstractWifeServices;
            this.abstractDaughterServices = abstractDaughterServices;
            this.abstractMasterDistrictServices = abstractMasterDistrictServices;
            this.abstractSonWifeServices = abstractSonWifeServices;
            this.abstractSonChildServices = abstractSonChildServices;
            this.abstractMasterCityServices = abstractMasterCityServices;
            this.abstractMasterEducationServices = abstractMasterEducationServices;
        }
        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index(int Id = 0)
        {
            ViewBag.MasterBloodGroup = BindDropDowns("masterBloodGroup");
            ViewBag.MasterDistrict = BindDropDowns("masterDistrict");
            ViewBag.MasterEducation = BindDropDowns("masterEducation");
            ViewBag.MasterCity = BindDropDowns("masterCity");
            ViewBag.Id = Id;           
            return View();
        }

        [ActionName(Actions.AdvanceDirectory)]
        public ActionResult AdvanceDirectory(int Id = 0)
        {
            ViewBag.MasterBloodGroup = BindDropDowns("masterBloodGroup");
            ViewBag.MasterDistrict = BindDropDowns("masterDistrict");
            ViewBag.MasterEducation = BindDropDowns("masterEducation");
            ViewBag.MasterCity = BindDropDowns("masterCity");
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindAdvanceDirectoryData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int Gender = 0, int Married = 0, int Engaged = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                AbstractMainMember abstractMainMember = new MainMember();
                abstractMainMember.Married = Married;
                abstractMainMember.Gender = Gender;
                abstractMainMember.Engaged = Engaged;
                var model = abstractMainMemberServices.AdvanceDirectory(pageParam, search, Gender, Married, Engaged);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string FirstName = "", int EducationId = 0, string Gotra = "", int CityId = 0, int BloodGroupId = 0, int Gender = 0, int Married = 0, int Engaged = 0, string MemberNumber = "")
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                AbstractMainMember abstractMainMember = new MainMember();
                abstractMainMember.CityId = CityId;
                abstractMainMember.FirstName = FirstName;
                abstractMainMember.EducationId = EducationId;
                abstractMainMember.Gotra = Gotra;
                abstractMainMember.Married = Married;
                abstractMainMember.Gender = Gender;
                abstractMainMember.Engaged = Engaged;
                abstractMainMember.BloodGroupId = BloodGroupId;
                abstractMainMember.MemberNumber = MemberNumber;
                var model = abstractMainMemberServices.MainMember_All(pageParam, search, FirstName, EducationId, Gotra, CityId, BloodGroupId, Gender, Married, Engaged, MemberNumber);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindWifeData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int MainMemberId = 0, int BloodGroupId = 0, int Gender = 0, int Engaged = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                AbstractWife abstractWife = new Wife();
                abstractWife.BloodGroupId = BloodGroupId;
                abstractWife.Gender = Gender;
                abstractWife.Engaged = Engaged;
                var model = abstractWifeServices.Wife_ByMainMemberId(pageParam, search, MainMemberId, BloodGroupId, Engaged,Gender);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindSonData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int MainMemberId = 0, int BloodGroupId=0, int Gender=0, int Engaged=0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                AbstractSon abstractSon = new Son();
                abstractSon.BloodGroupId = BloodGroupId;
                abstractSon.Gender = Gender;
                abstractSon.Engaged = Engaged;
                    
                var model = abstractSonServices.Son_All(pageParam, search, MainMemberId, BloodGroupId, Engaged, Gender);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindDaughterData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int MainMemberId = 0, int BloodGroupId = 0, int Gender = 0, int Engaged = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                AbstractDaughter abstractDaughter = new Daughter();
                abstractDaughter.BloodGroupId = BloodGroupId;
                abstractDaughter.Engaged = Engaged;
                abstractDaughter.Gender = Gender;
                var model = abstractDaughterServices.Daughter_ByMainMemberId(pageParam, search, MainMemberId, BloodGroupId, Engaged, Gender);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult MainMemberDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.MasterBloodGroup = BindDropDowns("masterBloodGroup");
            ViewBag.MasterDistrict = BindDropDowns("masterDistrict");
            ViewBag.MasterEducation = BindDropDowns("masterEducation");
            ViewBag.MasterCity = BindDropDowns("masterCity");
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public JsonResult GetById(int Id)
        {
            // int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));

            SuccessResult<AbstractMainMember> successResult = abstractMainMemberServices.MainMember_ById(Id);
            ViewBag.MainMemberId = successResult.Item.Id;
            ViewBag.SonId = successResult.Item.SonId;
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                if (action == "masterDistrict")
                {
                    var model = abstractMasterDistrictServices.MasterDistrict_ByStateId(pageParam, 2);
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterBloodGroup")
                {
                    var model = abstractMasterBloodGroupServices.MasterBloodGroup_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterCity")
                {
                    var model = abstractMasterCityServices.MasterCity_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                else if (action == "masterEducation")
                {
                    var model = abstractMasterEducationServices.MasterEducation_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                    }
                }
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindSonChildData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int SonId = 0, int BloodGroupId = 0, int Gender = 0, int Engaged = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                AbstractSonChild abstractSonChild = new SonChild();
                abstractSonChild.BloodGroupId = BloodGroupId;
                abstractSonChild.Gender = Gender;
                abstractSonChild.Engaged = Engaged;
                var model = abstractSonChildServices.SonChild_All(pageParam, search, SonId, BloodGroupId, Engaged, Gender);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public JsonResult GetBySonId(int Id)
        //{
        //    //int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
        //    SuccessResult<AbstractSon> successResult = abstractSonServices.Son_ById(Id);
        //    return Json(successResult, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindSonWifeData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int SonId = 0, int BloodGroupId = 0, int Gender = 0, int Engaged = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);
                AbstractSonWife abstractSonWife = new SonWife();
                abstractSonWife.BloodGroupId = BloodGroupId;
                abstractSonWife.Gender = Gender;
                abstractSonWife.Engaged = Engaged;
                var model = abstractSonWifeServices.SonWife_All(pageParam, search, SonId, BloodGroupId, Engaged, Gender);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

    }
}