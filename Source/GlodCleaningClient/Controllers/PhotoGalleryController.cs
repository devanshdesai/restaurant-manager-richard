﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class PhotoGalleryController : Controller
    {
        public readonly AbstractPhotoGalleryServices abstractPhotoGalleryServices;

        public PhotoGalleryController(AbstractPhotoGalleryServices abstractPhotoGalleryServices)
        {

            this.abstractPhotoGalleryServices = abstractPhotoGalleryServices;
        }
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            var result = abstractPhotoGalleryServices.PhotoGallery_All(pageParam, "").Values.ToList();

            return View(result);
        }

    }
}