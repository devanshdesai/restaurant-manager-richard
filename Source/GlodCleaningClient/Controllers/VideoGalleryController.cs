﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class VideoGalleryController : Controller
    {
        public readonly AbstractVideoGalleryServices abstractVideoGalleryServices;

        public VideoGalleryController(AbstractVideoGalleryServices abstractVideoGalleryServices)
        {

            this.abstractVideoGalleryServices = abstractVideoGalleryServices;
        }
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            var result = abstractVideoGalleryServices.VideoGallery_All(pageParam, "").Values.ToList();

            return View(result);
        }

    }
}