﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class LatestAnnouncementController : Controller
    {
        public readonly AbstractLatestAnnouncementServices abstractLatestAnnouncementServices;

        public LatestAnnouncementController(AbstractLatestAnnouncementServices abstractLatestAnnouncementServices)
        {

            this.abstractLatestAnnouncementServices = abstractLatestAnnouncementServices;
        }
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            var result = abstractLatestAnnouncementServices.LatestAnnouncement_All(pageParam, "").Values.ToList();

            return View(result);
        }

    }
}