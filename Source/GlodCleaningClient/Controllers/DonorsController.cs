﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class DonorsController : Controller
    {
        public readonly AbstractDonorsServices abstractDonorsServices;

        public DonorsController(AbstractDonorsServices abstractDonorsServices)
        {

            this.abstractDonorsServices = abstractDonorsServices;
        }
        public ActionResult Index()
        {
            //Donors committeeMember = new Donors();

            PageParam pageParam = new PageParam();
            var result = abstractDonorsServices.Donors_All(pageParam, "", 0).Values.ToList();

            return View(result);
        }
    }
}