﻿using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.V1;
using GlodCleaning.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlodCleaningClient.Controllers
{
    public class CommitteeMemberController : Controller
    {
        public readonly AbstractCommitteeMemberServices abstractCommitteeMemberServices;

        public CommitteeMemberController( AbstractCommitteeMemberServices abstractCommitteeMemberServices)
        {
            
            this.abstractCommitteeMemberServices = abstractCommitteeMemberServices;
        }
        public ActionResult Index()
        {
            //CommitteeMember committeeMember = new CommitteeMember();
            
            PageParam pageParam = new PageParam();           
            var result = abstractCommitteeMemberServices.CommitteeMember_All(pageParam, "",0).Values.ToList();

            return View(result);
        }
        
    }
}