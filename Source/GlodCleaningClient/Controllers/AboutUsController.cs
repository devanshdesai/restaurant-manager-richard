﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.V1;
using GlodCleaning.Services.Contract;

namespace GlodCleaningClient.Controllers
{
    public class AboutUsController : Controller
    {
        public readonly AbstractPageImagesServices abstractPageImagesServices;

        public AboutUsController(AbstractPageImagesServices abstractPageImagesServices)
        {

            this.abstractPageImagesServices = abstractPageImagesServices;
        }
        public ActionResult Index()
        {
            PageParam pageParam = new PageParam();
            var result = abstractPageImagesServices.PageImages_All(pageParam, "", 2).Values.ToList();

            return View(result);
        }
        
    }
}