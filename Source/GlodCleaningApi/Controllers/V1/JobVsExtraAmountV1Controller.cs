﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class JobVsExtraAmountV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobVsExtraAmountServices  abstractJobVsExtraAmountServices;

        #endregion

        #region Cnstr
        public JobVsExtraAmountV1Controller(AbstractJobVsExtraAmountServices abstractJobVsExtraAmountServices)
        {
            this.abstractJobVsExtraAmountServices = abstractJobVsExtraAmountServices;
        }
        #endregion

        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsExtraAmount_Upsert")]
        public async Task<IHttpActionResult> JobVsExtraAmount_Upsert(JobVsExtraAmount jobVsExtraAmount)
        {
            var quote = abstractJobVsExtraAmountServices.JobVsExtraAmount_Upsert(jobVsExtraAmount);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsExtraAmount_All")]
        public async Task<IHttpActionResult> JobVsExtraAmount_ByJobId(PageParam pageParam,string search="",int JobId=0)
        {
            var quote = abstractJobVsExtraAmountServices.JobVsExtraAmount_ByJobId(pageParam,search,JobId);
            return this.Content((HttpStatusCode)200, quote);
        }
     
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsExtraAmount_Delete")]
        public async Task<IHttpActionResult> JobVsExtraAmount_Delete(int Id)
        {
            var quote = abstractJobVsExtraAmountServices.JobVsExtraAmount_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        

    }
}
