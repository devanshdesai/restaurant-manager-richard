﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class CustomerVsAddressV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerVsAddressServices abstractCustomerVsAddressServices;

        #endregion

        #region Cnstr
        public CustomerVsAddressV1Controller(AbstractCustomerVsAddressServices abstractCustomerVsAddressServices)
        {
            this.abstractCustomerVsAddressServices = abstractCustomerVsAddressServices;
        }
        #endregion

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsAddress_Upsert")]
        public async Task<IHttpActionResult> CustomerVsAddress_Upsert(CustomerVsAddress customerVsAddress)
        {
            var quote = abstractCustomerVsAddressServices.CustomerVsAddress_Upsert(customerVsAddress);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsAddress_ById")]
        public async Task<IHttpActionResult> CustomerVsAddress_ById(int Id)
        {
            var quote = abstractCustomerVsAddressServices.CustomerVsAddress_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsAddress_All")]
        public async Task<IHttpActionResult> CustomerVsAddress_ByCustomerId(PageParam pageParam,string search="",int CustomerId = 0)
        {
            var quote = abstractCustomerVsAddressServices.CustomerVsAddress_ByCustomerId(pageParam,search,CustomerId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsAddress_IsDefault")]
        public async Task<IHttpActionResult> CustomerVsAddress_IsDefault(int Id)
        {
            var quote = abstractCustomerVsAddressServices.CustomerVsAddress_IsDefault(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsAddress_Delete")]
        public async Task<IHttpActionResult> CustomerVsAddress_Delete(CustomerVsAddress customerVsAddress)
        {
            var quote = abstractCustomerVsAddressServices.CustomerVsAddress_Delete(customerVsAddress);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
