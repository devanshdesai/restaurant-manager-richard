﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class JobV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobServices  abstractJobServices;
        private readonly AbstractMasterExtraServices abstractMasterExtraServices;
        #endregion

        #region Cnstr
        public JobV1Controller(AbstractJobServices abstractJobServices, AbstractMasterExtraServices abstractMasterExtraServices)
        {
            this.abstractJobServices = abstractJobServices;
            this.abstractMasterExtraServices = abstractMasterExtraServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_Upsert")]
        public async Task<IHttpActionResult> Job_Upsert(Job job)
        {
            var quote = abstractJobServices.Job_Upsert(job);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_All")]
        public async Task<IHttpActionResult> Job_All(PageParam pageParam, int CustomerId = 0, int CleanerId = 0, int JobStatusId = 0, string JobAssignedDateTime = "", string JobStartDateTime = "", string JobEndDateTime = "", int CountryId = 0, int StateId = 0, int CityId = 0, int SuburbId = 0)
        {
            var quote = abstractJobServices.Job_All(pageParam,CustomerId,CleanerId,JobStatusId,JobAssignedDateTime,JobStartDateTime,JobEndDateTime,CountryId,StateId,CityId,SuburbId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_ById")]
        public async Task<IHttpActionResult> Job_ById(int Id)
        {
            var quote = abstractJobServices.Job_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_AssignCleaner")]
        public async Task<IHttpActionResult> Job_AssignCleaner(Job job)
        {
            var quote = abstractJobServices.Job_AssignCleaner(job);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_ChangeStatus")]
        public async Task<IHttpActionResult> Job_ChangeStatus(Job job)
        {
            var quote = abstractJobServices.Job_ChangeStatus(job);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_Start")]
        public async Task<IHttpActionResult> Job_Start(int Id)
        {
            var quote = abstractJobServices.Job_Start(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_End")]
        public async Task<IHttpActionResult> Job_End(int Id)
        {
            var quote = abstractJobServices.Job_End(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_Price")]
        public async Task<IHttpActionResult> Job_Price(List<JobVsExtras> jobVsExtrasItems,int JobServiceId = 0,string propertyLayout="",string aboutCompany="",string propertyCondition = "", string oftenCleaning = "",string day = "",string parkingValue="",int floorQty=0,int bedroomQty=0, int bathroomQty=0,decimal tip = 0)
        {
            JobPrice jobPrice = new JobPrice();
                
            if (JobServiceId == 1)
            {
                if (propertyLayout == "Studio")
                {
                    jobPrice.propertyLayoutAmt = 99;
                    jobPrice.floorAmt = jobPrice.bedroomAmt = jobPrice.bathroomAmt = 0;
                }
                else
                {
                    jobPrice.propertyLayoutAmt = 0;
                    if (floorQty == 1)
                    {
                        jobPrice.floorAmt = 39;
                    }
                    else if (floorQty > 1)
                    {
                        jobPrice.floorAmt = 39 + ((floorQty-1) * 15);
                    }

                    if (bedroomQty == 1)
                    {
                        jobPrice.bedroomAmt = 35;
                    }
                    else if (bedroomQty > 1)
                    {
                        jobPrice.bedroomAmt = 35 + ((bedroomQty-1) * 35);
                    }

                    if (bathroomQty == 1)
                    {
                        jobPrice.bathroomAmt = 35;
                    }
                    else if (bathroomQty > 1)
                    {
                        jobPrice.bathroomAmt = 35 + ((bathroomQty-1) * 20);
                    }
                    jobPrice.propertyLayoutAmt = jobPrice.floorAmt + jobPrice.bedroomAmt + jobPrice.bathroomAmt;
                }

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                var jobExtraItems = abstractMasterExtraServices.MasterExtra_All(pageParam, 1, "");
                jobPrice.jobVsExtras = new List<JobVsExtras>();

                foreach (var item in jobExtraItems.Values)
                {
                    foreach(var passedItems in jobVsExtrasItems)
                    {
                        if(passedItems.Id == item.Id)
                        {
                            JobVsExtras jobVsExtras = new JobVsExtras();
                            jobVsExtras.Id = passedItems.Id;
                            jobVsExtras.Qty = passedItems.Qty;
                            if (passedItems.Qty == 1)
                            {
                                jobVsExtras.Amt = item.BasePrice;
                            }
                            else if (passedItems.Qty > 1)
                            {
                                jobVsExtras.Amt = item.BasePrice + (item.BasePrice * (passedItems.Qty - 1));
                            }
                            jobPrice.jobVsExtras.Add(jobVsExtras);
                            jobPrice.extraAmt = jobPrice.extraAmt + jobVsExtras.Amt;
                        }
                    }
                }

                jobPrice.baseAmt = jobPrice.propertyLayoutAmt + jobPrice.extraAmt;

                if (oftenCleaning == "One Time")
                {
                    jobPrice.discountAmt = 0;
                    jobPrice.discountPerc = 0;
                }
                else if (oftenCleaning == "Weekly")
                {
                    jobPrice.discountAmt = ((jobPrice.baseAmt * 20) / 100);
                    jobPrice.discountPerc = 20;
                }
                else if (oftenCleaning == "Fortnightly")
                {
                    jobPrice.discountAmt = ((jobPrice.baseAmt * 15) / 100);
                    jobPrice.discountPerc = 15;
                }
                else if (oftenCleaning == "Monthly")
                {
                    jobPrice.discountAmt = ((jobPrice.baseAmt * 10) / 100);
                    jobPrice.discountPerc = 10;
                }

                if(day == "Sunday")
                {
                    jobPrice.calSurchargeAmt = ((jobPrice.baseAmt * 20) / 100);
                }
                else
                {
                    jobPrice.calSurchargeAmt = 0;
                }

                jobPrice.cleanerTipAmt = tip;

                jobPrice.calPropertyCondition = 0;

                if (parkingValue == "Paid Street Parking")
                {
                    jobPrice.parkingChargeAmt = 10;
                }
                else
                {
                    jobPrice.parkingChargeAmt = 0;
                }

                jobPrice.totalAmt = jobPrice.baseAmt + jobPrice.calSurchargeAmt + jobPrice.parkingChargeAmt + jobPrice.cleanerTipAmt - jobPrice.discountAmt;
            }

            if (JobServiceId == 2)
            {
                if (propertyLayout == "Studio")
                {
                    jobPrice.propertyLayoutAmt = 199;
                    jobPrice.floorAmt = jobPrice.bedroomAmt = jobPrice.bathroomAmt = 0;
                }
                else
                {
                    jobPrice.propertyLayoutAmt = 0;
                    if (floorQty == 1)
                    {
                        jobPrice.floorAmt = 85;
                    }
                    else if (floorQty > 1)
                    {
                        jobPrice.floorAmt = 85 + ((floorQty - 1) * 20);
                    }

                    if (bedroomQty == 1)
                    {
                        jobPrice.bedroomAmt = 85;
                    }
                    else if (bedroomQty > 1)
                    {
                        jobPrice.bedroomAmt = 85 + ((bedroomQty - 1) * 50);
                    }

                    if (bathroomQty == 1)
                    {
                        jobPrice.bathroomAmt = 85;
                    }
                    else if (bathroomQty > 1)
                    {
                        jobPrice.bathroomAmt = 85 + ((bathroomQty - 1) * 30);
                    }
                    jobPrice.propertyLayoutAmt = jobPrice.floorAmt + jobPrice.bedroomAmt + jobPrice.bathroomAmt;
                }

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                var jobExtraItems = abstractMasterExtraServices.MasterExtra_All(pageParam, 2, "");
                jobPrice.jobVsExtras = new List<JobVsExtras>();

                foreach (var item in jobExtraItems.Values)
                {
                    foreach (var passedItems in jobVsExtrasItems)
                    {
                        if (passedItems.Id == item.Id)
                        {
                            JobVsExtras jobVsExtras = new JobVsExtras();
                            jobVsExtras.Id = passedItems.Id;
                            jobVsExtras.Qty = passedItems.Qty;
                            if (passedItems.Qty == 1)
                            {
                                jobVsExtras.Amt = item.BasePrice;
                            }
                            else if (passedItems.Qty > 1)
                            {
                                jobVsExtras.Amt = item.BasePrice + (item.BasePrice * (passedItems.Qty - 1));
                            }
                            jobPrice.jobVsExtras.Add(jobVsExtras);
                            jobPrice.extraAmt = jobPrice.extraAmt + jobVsExtras.Amt;
                        }
                    }
                }

                jobPrice.baseAmt = jobPrice.propertyLayoutAmt + jobPrice.extraAmt;

               
                jobPrice.discountAmt = 0;
                jobPrice.discountPerc = 0;
               

                if (day == "Sunday")
                {
                    jobPrice.calSurchargeAmt = ((jobPrice.baseAmt * 20) / 100);
                }
                else
                {
                    jobPrice.calSurchargeAmt = 0;
                }


                if (propertyCondition == "Furnished")
                {
                    jobPrice.calPropertyCondition = ((jobPrice.baseAmt * 20) / 100);
                }
                else
                {
                    jobPrice.calPropertyCondition = 0;
                }

                jobPrice.cleanerTipAmt = tip;

                if (parkingValue == "Paid Street Parking")
                {
                    jobPrice.parkingChargeAmt = 10;
                }
                else
                {
                    jobPrice.parkingChargeAmt = 0;
                }

                jobPrice.totalAmt = jobPrice.baseAmt + jobPrice.calSurchargeAmt + jobPrice.calPropertyCondition + jobPrice.parkingChargeAmt + jobPrice.cleanerTipAmt +  - jobPrice.discountAmt;
            }

            if (JobServiceId == 3)
            {
                //if (aboutCompany == "Studio")
                //{
                //    jobPrice.propertyLayoutAmt = 199;
                //    jobPrice.floorAmt = jobPrice.bedroomAmt = jobPrice.bathroomAmt = 0;
                //}
                //else
                //{
                //    jobPrice.propertyLayoutAmt = 0;
                //    if (floorQty == 1)
                //    {
                //        jobPrice.floorAmt = 85;
                //    }
                //    else if (floorQty > 1)
                //    {
                //        jobPrice.floorAmt = 85 + ((floorQty - 1) * 20);
                //    }

                //    if (bedroomQty == 1)
                //    {
                //        jobPrice.bedroomAmt = 85;
                //    }
                //    else if (bedroomQty > 1)
                //    {
                //        jobPrice.bedroomAmt = 85 + ((bedroomQty - 1) * 50);
                //    }

                //    if (bathroomQty == 1)
                //    {
                //        jobPrice.bathroomAmt = 85;
                //    }
                //    else if (bathroomQty > 1)
                //    {
                //        jobPrice.bathroomAmt = 85 + ((bathroomQty - 1) * 30);
                //    }
                //    jobPrice.propertyLayoutAmt = jobPrice.floorAmt + jobPrice.bedroomAmt + jobPrice.bathroomAmt;
                //}

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;
                var jobExtraItems = abstractMasterExtraServices.MasterExtra_All(pageParam, 3, "");
                jobPrice.jobVsExtras = new List<JobVsExtras>();

                foreach (var item in jobExtraItems.Values)
                {
                    foreach (var passedItems in jobVsExtrasItems)
                    {
                        if (passedItems.Id == item.Id)
                        {
                            JobVsExtras jobVsExtras = new JobVsExtras();
                            jobVsExtras.Id = passedItems.Id;
                            jobVsExtras.Qty = passedItems.Qty;
                            if (passedItems.Qty == 1)
                            {
                                jobVsExtras.Amt = item.BasePrice;
                            }
                            else if (passedItems.Qty > 1)
                            {
                                jobVsExtras.Amt = item.BasePrice + (item.BasePrice * (passedItems.Qty - 1));
                            }
                            jobPrice.jobVsExtras.Add(jobVsExtras);
                            jobPrice.extraAmt = jobPrice.extraAmt + jobVsExtras.Amt;
                        }
                    }
                }

                jobPrice.baseAmt = jobPrice.aboutCompanyAmt + jobPrice.extraAmt;


                jobPrice.discountAmt = 0;
                jobPrice.discountPerc = 0;

                if (oftenCleaning == "5 days a week")
                {
                    jobPrice.discountAmt = ((jobPrice.baseAmt * 20) / 100);
                    jobPrice.discountPerc = 20;
                }
                else if (oftenCleaning == "3 days a week")
                {
                    jobPrice.discountAmt = ((jobPrice.baseAmt * 15) / 100);
                    jobPrice.discountPerc = 15;
                }
                else if (oftenCleaning == "2 days a week")
                {
                    jobPrice.discountAmt = ((jobPrice.baseAmt * 10) / 100);
                    jobPrice.discountPerc = 10;
                }
                else if (oftenCleaning == "Once a week")
                {
                    jobPrice.discountAmt = ((jobPrice.baseAmt * 5) / 100);
                    jobPrice.discountPerc = 5;
                }
                else if (oftenCleaning == "Fortnightly")
                {
                    jobPrice.discountAmt = 0;
                    jobPrice.discountPerc = 0;
                }
                else if (oftenCleaning == "Monthly (Every 4 weeks)")
                {
                    jobPrice.discountAmt = 0;
                    jobPrice.discountPerc = 0;
                }

                jobPrice.calSurchargeAmt = 0;
                jobPrice.calPropertyCondition = 0;

                jobPrice.cleanerTipAmt = tip;

                if (parkingValue == "Paid Street Parking")
                {
                    jobPrice.parkingChargeAmt = 10;
                }
                else
                {
                    jobPrice.parkingChargeAmt = 0;
                }

                jobPrice.totalAmt = jobPrice.baseAmt + jobPrice.calSurchargeAmt + jobPrice.calPropertyCondition + jobPrice.parkingChargeAmt + jobPrice.cleanerTipAmt + -jobPrice.discountAmt;
            }

            return this.Content((HttpStatusCode)200, jobPrice);
        }

        public class JobPrice
        {
            public decimal propertyLayoutAmt { get; set; }
            public decimal aboutCompanyAmt { get; set; }
            public decimal floorAmt { get; set; }
            public decimal bedroomAmt { get; set; }
            public decimal bathroomAmt { get; set; }
            public decimal extraAmt { get; set; }
            public decimal baseAmt { get; set; }
            public decimal discountAmt { get; set; }
            public decimal discountPerc { get; set; }
            public decimal calPropertyCondition { get; set; }
            public decimal calSurchargeAmt { get; set; }
            public decimal parkingChargeAmt { get; set; }
            public decimal cleanerTipAmt { get; set; }
            public decimal totalAmt { get; set; }
            public List<JobVsExtras> jobVsExtras { get; set; }
        }
    }
}
