﻿using GlodCleaning.Common;
using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class WifeV1Controller : AbstractBaseController
    {
        private readonly AbstractWifeServices abstractWifeServices;

        public WifeV1Controller(AbstractWifeServices abstractWifeServices)
        {
            this.abstractWifeServices=abstractWifeServices;
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Wife_Upsert")]
        public async Task<IHttpActionResult> Wife_Upsert(AbstractWife abstractWife)
        {
            var quote = abstractWifeServices.Wife_Upsert(abstractWife);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Wife_ById")]
        public async Task<IHttpActionResult> Wife_ById(int Id)
        {
            var quote = abstractWifeServices.Wife_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Wife_Delete")]
        public async Task<IHttpActionResult> Wife_Delete(int Id)
        {
            var quote = abstractWifeServices.Wife_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("Wife_All")]
        public async Task<IHttpActionResult> Wife_All(PageParam pageParam, string Search, int MainMemberId, int BloodGroupId, int Gender, int Engaged)
        {
            var quote = abstractWifeServices.Wife_All(pageParam, Search,MainMemberId,BloodGroupId,Engaged,Gender);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}