﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class SupportV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractSupportServices abstractSupportServices;

        #endregion

        #region Cnstr
        public SupportV1Controller(AbstractSupportServices abstractSupportServices)
        {
            this.abstractSupportServices = abstractSupportServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Support_Upsert")]
        public async Task<IHttpActionResult> Support_Upsert(Support support)
        {
            var quote = abstractSupportServices.Support_Upsert(support);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Support_Close")]
        public async Task<IHttpActionResult> Support_Close(Support support)
        {
            var quote = abstractSupportServices.Support_Close(support);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Support_All")]
        public async Task<IHttpActionResult> Support_All(PageParam pageParam, int CustomerId = 0, int CleanerId = 0, int JobId = 0, int StatusId= 0)
        {
            var quote = abstractSupportServices.Support_All(pageParam, CustomerId, CleanerId, JobId, StatusId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Support_ById")]
        public async Task<IHttpActionResult> Support_ById(int Id)
        {
            var quote = abstractSupportServices.Support_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        


    }
}
