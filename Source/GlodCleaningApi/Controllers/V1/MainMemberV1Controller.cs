﻿using GlodCleaning.Common;
using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MainMemberV1Controller : AbstractBaseController
    {
        private readonly AbstractMainMemberServices abstractMainMemberServices;

        public MainMemberV1Controller(AbstractMainMemberServices abstractMainMemberServices)
        {
            this.abstractMainMemberServices=abstractMainMemberServices;
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MainMember_Upsert")]
        public async Task<IHttpActionResult> MainMember_Upsert(AbstractMainMember abstractMainMember)
        {
            var quote = abstractMainMemberServices.MainMember_Upsert(abstractMainMember);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("MainMember_ById")]
        public async Task<IHttpActionResult> MainMember_ById(int Id)
        {
            var quote = abstractMainMemberServices.MainMember_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("MainMember_Delete")]
        public async Task<IHttpActionResult> MainMember_Delete(int CarrierId)
        {
            var quote = abstractMainMemberServices.MainMember_Delete(CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("MainMember_All")]
        public async Task<IHttpActionResult> MainMember_All(PageParam pageParam, string Search,string FirstName,int EducationId,string Gotra,int CityId,int BloodGroupId,int Gender,int Married,int Engaged)
        {
            var quote = abstractMainMemberServices.MainMember_All(pageParam, Search, FirstName, EducationId, Gotra, CityId, BloodGroupId, Gender, Married, Engaged);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}