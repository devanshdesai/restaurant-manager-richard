﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterServiceTypesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterServiceTypesServices abstractMasterServiceTypesServices;

        #endregion

        #region Cnstr
        public MasterServiceTypesV1Controller(AbstractMasterServiceTypesServices abstractMasterServiceTypesServices)
        {
            this.abstractMasterServiceTypesServices = abstractMasterServiceTypesServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceTypes_All")]
        public async Task<IHttpActionResult> MasterServiceTypes_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterServiceTypesServices.MasterServiceTypes_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}