﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterExtraV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterExtraServices  abstractMasterExtraServices;

        #endregion

        #region Cnstr
        public MasterExtraV1Controller(AbstractMasterExtraServices abstractMasterExtraServices)
        {
            this.abstractMasterExtraServices = abstractMasterExtraServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterExtra_All")]
        public async Task<IHttpActionResult> MasterExtra_All(PageParam pageParam,int ServiceTypeId,string search = "")
        {
            var quote = abstractMasterExtraServices.MasterExtra_All(pageParam, ServiceTypeId, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
