﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterDocumentTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterDocumentTypeServices abstractMasterDocumentTypeServices;

        #endregion

        #region Cnstr
        public MasterDocumentTypeV1Controller(AbstractMasterDocumentTypeServices abstractMasterDocumentTypeServices)
        {
            this.abstractMasterDocumentTypeServices = abstractMasterDocumentTypeServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterDocumentType_All")]
        public async Task<IHttpActionResult> MasterDocumentType_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterDocumentTypeServices.MasterDocumentType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}