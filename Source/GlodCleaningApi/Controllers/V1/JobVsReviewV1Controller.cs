﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class JobVsReviewV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobVsReviewServices  abstractJobVsReviewServices;

        #endregion

        #region Cnstr
        public JobVsReviewV1Controller(AbstractJobVsReviewServices abstractJobVsReviewServices)
        {
            this.abstractJobVsReviewServices = abstractJobVsReviewServices;
        }
        #endregion

        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsReview_Upsert")]
        public async Task<IHttpActionResult> JobVsReview_Upsert(JobVsReview jobVsReview)
        {
            var quote = abstractJobVsReviewServices.JobVsReview_Upsert(jobVsReview);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsReview_ByJobId")]
        public async Task<IHttpActionResult> JobVsReview_ByJobId(PageParam pageParam,int JobId=0, int CustomerId = 0, int CleanerId = 0)
        {
            var quote = abstractJobVsReviewServices.JobVsReview_ByJobId(pageParam,JobId, CustomerId, CleanerId);
            return this.Content((HttpStatusCode)200, quote);
        }
     
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsReview_ActInact")]
        public async Task<IHttpActionResult> JobVsReview_ActInact(int Id)
        {
            var quote = abstractJobVsReviewServices.JobVsReview_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsReview_ById")]
        public async Task<IHttpActionResult> JobVsReview_ById(int Id)
        {
            var quote = abstractJobVsReviewServices.JobVsReview_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }



    }
}
