﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class TransactionV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTransactionServices abstractTransactionServices;

        #endregion

        #region Cnstr
        public TransactionV1Controller(AbstractTransactionServices abstractTransactionServices)
        {
            this.abstractTransactionServices = abstractTransactionServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Transaction_Upsert")]
        public async Task<IHttpActionResult> Transaction_Upsert(Transaction transaction)
        {
            var quote = abstractTransactionServices.Transaction_Upsert(transaction);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Transaction_ById")]
        public async Task<IHttpActionResult> Transaction_ById(int Id)
        {
            var quote = abstractTransactionServices.Transaction_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Transaction_All")]
        public async Task<IHttpActionResult> Transaction_All(PageParam pageParam, string search, int JobId, string TransactionNo, int UserType, int UserId, DateTime CreatedDate)
        {
            var quote = abstractTransactionServices.Transaction_All(pageParam, search, JobId, TransactionNo, UserType, UserId, CreatedDate);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}