﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterAddressTypeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterAddressTypeServices abstractMasterAddressTypeServices;

        #endregion

        #region Cnstr
        public MasterAddressTypeV1Controller(AbstractMasterAddressTypeServices abstractMasterAddressTypeServices)
        {
            this.abstractMasterAddressTypeServices = abstractMasterAddressTypeServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterAddressType_All")]
        public async Task<IHttpActionResult> MasterAddressType_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterAddressTypeServices.MasterAddressType_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}