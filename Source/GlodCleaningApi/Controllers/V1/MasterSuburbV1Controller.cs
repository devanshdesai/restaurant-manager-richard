﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterSuburbV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterSuburbServices abstractMasterSuburbServices;

        #endregion

        #region Cnstr
        public MasterSuburbV1Controller(AbstractMasterSuburbServices abstractMasterSuburbServices)
        {
            this.abstractMasterSuburbServices = abstractMasterSuburbServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterSuburb_ByCityId")]
        public async Task<IHttpActionResult> MasterSuburb_ByCityId(PageParam pageParam, int CityId, string search="")
        {
            var quote = abstractMasterSuburbServices.MasterSuburb_ByCityId(pageParam, search, CityId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}