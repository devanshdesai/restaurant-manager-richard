﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using System.IO;

namespace GlodCleaningApi.Controllers.V1
{
    public class ProvidersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractProvidersServices abstractProvidersServices;

        #endregion

        #region Cnstr
        public ProvidersV1Controller(AbstractProvidersServices abstractProvidersServices)
        {
            this.abstractProvidersServices = abstractProvidersServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_Login")]
        public async Task<IHttpActionResult> Providers_Login(Providers providers)
        {
            var quote = abstractProvidersServices.Providers_Login(providers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_Logout")]
        public async Task<IHttpActionResult> Providers_Logout(int Id)
        {
            var quote = abstractProvidersServices.Providers_Logout(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_Upsert")]
        public async Task<IHttpActionResult> Providers_Upsert(Providers providers)
        {
            var quote = abstractProvidersServices.Providers_Upsert(providers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_ById")]
        public async Task<IHttpActionResult> Providers_ById(int Id)
        {
            var quote = abstractProvidersServices.Providers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_All")]
        public async Task<IHttpActionResult> Providers_All(PageParam pageParam, string search="")
        {
            var quote = abstractProvidersServices.Providers_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_ActInact")]
        public async Task<IHttpActionResult> Providers_ActInact(int Id)
        {
            var quote = abstractProvidersServices.Providers_ActInact(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_Approve")]
        public async Task<IHttpActionResult> Providers_AdminApproved(int Id)
        {
            var quote = abstractProvidersServices.Providers_AdminApproved(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_ChangePassword")]
        public async Task<IHttpActionResult> Providers_ChangePassword(Providers providers)
        {
            var quote = abstractProvidersServices.Providers_ChangePassword(providers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Providers_ProfileUpdate")]
        public async Task<IHttpActionResult> Providers_ProfileUpdate()
        {
            Providers providers = new Providers();
            var httpRequest = HttpContext.Current.Request;
            providers.Id = Convert.ToInt32(httpRequest.Params["Id"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CleanerImages/" + providers.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                providers.AvatarPhotoCopy = basePath + fileName;
            }
            var quote = abstractProvidersServices.Providers_ProfileUpdate(providers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}