﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterStateV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterStateServices abstractMasterStateServices;

        #endregion

        #region Cnstr
        public MasterStateV1Controller(AbstractMasterStateServices abstractMasterStateServices)
        {
            this.abstractMasterStateServices = abstractMasterStateServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterState_ByCountryId")]
        public async Task<IHttpActionResult> MasterState_ByCountryId(PageParam pageParam, int CountryId, string search="")
        {
            var quote = abstractMasterStateServices.MasterState_ByCountryId(pageParam, search, CountryId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}