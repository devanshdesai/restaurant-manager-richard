﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class CleanerVsDetailsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCleanerVsDetailsServices abstractCleanerVsDetailsServices;

        #endregion

        #region Cnstr
        public CleanerVsDetailsV1Controller(AbstractCleanerVsDetailsServices abstractCleanerVsDetailsServices)
        {
            this.abstractCleanerVsDetailsServices = abstractCleanerVsDetailsServices;
        }
        #endregion

       
        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsDetails_Upsert")]
        public async Task<IHttpActionResult> CleanerVsDetails_Upsert(CleanerVsDetails cleanerVsdetails)
        {
            var quote = abstractCleanerVsDetailsServices.CleanerVsDetails_Upsert(cleanerVsdetails);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsDetails_ByCleanerId")]
        public async Task<IHttpActionResult> CleanerVsDetails_ByCleanerId(int CleanerId)
        {
            var quote = abstractCleanerVsDetailsServices.CleanerVsDetails_ByCleanerId(CleanerId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
       
    }
}