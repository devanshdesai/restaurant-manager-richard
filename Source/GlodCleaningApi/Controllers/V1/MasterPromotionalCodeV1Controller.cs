﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterPromotionalCodeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterPromotionalCodeServices abstractMasterPromotionalCodeServices;

        #endregion

        #region Cnstr
        public MasterPromotionalCodeV1Controller(AbstractMasterPromotionalCodeServices abstractMasterPromotionalCodeServices)
        {
            this.abstractMasterPromotionalCodeServices = abstractMasterPromotionalCodeServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPromotionalCode_Upsert")]
        public async Task<IHttpActionResult> MasterPromotionalCode_Upsert(MasterPromotionalCode masterpromotionalcode)
        {
            var quote = abstractMasterPromotionalCodeServices.MasterPromotionalCode_Upsert(masterpromotionalcode);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPromotionalCode_ById")]
        public async Task<IHttpActionResult> MasterPromotionalCode_ById(int Id)
        {
            var quote = abstractMasterPromotionalCodeServices.MasterPromotionalCode_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPromotionalCode_ByPromoCode")]
        public async Task<IHttpActionResult> MasterPromotionalCode_ByPromoCode(string Promocode)
        {
            var quote = abstractMasterPromotionalCodeServices.MasterPromotionalCode_ByPromoCode(Promocode);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPromotionalCode_All")]
        public async Task<IHttpActionResult> MasterPromotionalCode_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterPromotionalCodeServices.MasterPromotionalCode_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPromotionalCode_Delete")]
        public async Task<IHttpActionResult> MasterPromotionalCode_Delete(int Id)
        {
            var quote = abstractMasterPromotionalCodeServices.MasterPromotionalCode_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPromotionalCode_ActInact")]
        public async Task<IHttpActionResult> MasterPromotionalCode_ActInact(int Id)
        {
            var quote = abstractMasterPromotionalCodeServices.MasterPromotionalCode_ActInact(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}