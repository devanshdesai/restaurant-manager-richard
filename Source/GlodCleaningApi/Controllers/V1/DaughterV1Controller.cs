﻿using GlodCleaning.Common;
using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.V1;


namespace GlodCleaningApi.Controllers.V1
{
    public class DaughterV1Controller : AbstractBaseController
    {
        private readonly AbstractDaughterServices abstractDaughterServices;

        public DaughterV1Controller(AbstractDaughterServices abstractDaughterServices)
        {
            this.abstractDaughterServices=abstractDaughterServices;
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Daughter_Upsert")]
        public async Task<IHttpActionResult> Daughter_Upsert(AbstractDaughter abstractDaughter)
        {
            var quote = abstractDaughterServices.Daughter_Upsert(abstractDaughter);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Daughter_ById")]
        public async Task<IHttpActionResult> Daughter_ById(int Id)
        {
            var quote = abstractDaughterServices.Daughter_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Daughter_Delete")]
        public async Task<IHttpActionResult> Daughter_Delete(int Id)
        {
            var quote = abstractDaughterServices.Daughter_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("Daughter_All")]
        public async Task<IHttpActionResult> Daughter_All(PageParam pageParam, string Search, int MainMemberId, int BloodGroupId, int Gender, int Engaged)
        {
            var quote = abstractDaughterServices.Daughter_All(pageParam, Search, MainMemberId, BloodGroupId, Engaged, Gender);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}