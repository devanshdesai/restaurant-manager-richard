﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using System.IO;

namespace GlodCleaningApi.Controllers.V1
{
    public class CustomersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomersServices  abstractCustomersServices;

        #endregion

        #region Cnstr
        public CustomersV1Controller(AbstractCustomersServices abstractCustomersServices)
        {
            this.abstractCustomersServices = abstractCustomersServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_Login")]
        public async Task<IHttpActionResult> Customers_Login(Customers customers)
        {
            var quote = abstractCustomersServices.Customers_Login(customers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_Logout")]
        public async Task<IHttpActionResult> Customers_Logout(int Id)
        {
            var quote = abstractCustomersServices.Customers_Logout(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_Upsert")]
        public async Task<IHttpActionResult> Customers_Upsert(Customers customers)
        {
            var quote = abstractCustomersServices.Customers_Upsert(customers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_ById")]
        public async Task<IHttpActionResult> Customers_ById(int Id)
        {
            var quote = abstractCustomersServices.Customers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_All")]
        public async Task<IHttpActionResult> Customers_All(PageParam pageParam,string search="")
        {
            var quote = abstractCustomersServices.Customers_All(pageParam,search);
            return this.Content((HttpStatusCode)200, quote);
        }
     
        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_ActInact")]
        public async Task<IHttpActionResult> Customers_ActInact(int Id)
        {
            var quote = abstractCustomersServices.Customers_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_ChangePassword")]
        public async Task<IHttpActionResult> Customers_ChangePassword(Customers customers)
        {
            var quote = abstractCustomersServices.Customers_ChangePassword(customers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_ProfileUpdate")]
        public async Task<IHttpActionResult> Customers_ProfileUpdate()
        {
            Customers customers = new Customers();
            var httpRequest = HttpContext.Current.Request;
            customers.Id = Convert.ToInt32(httpRequest.Params["Id"]);
           
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CustomerImages/" + customers.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                customers.AvatarPhotoCopy = basePath + fileName;
            }
            var quote = abstractCustomersServices.Customers_ProfileUpdate(customers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
