﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterBannerV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterBannerServices abstractMasterBannerServices;

        #endregion

        #region Cnstr
        public MasterBannerV1Controller(AbstractMasterBannerServices abstractMasterBannerServices)
        {
            this.abstractMasterBannerServices = abstractMasterBannerServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterBanner_Upsert")]
        public async Task<IHttpActionResult> MasterBanner_Upsert(MasterBanner masterbanner)
        {
            var quote = abstractMasterBannerServices.MasterBanner_Upsert(masterbanner);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterBanner_ById")]
        public async Task<IHttpActionResult> MasterBanner_ById(int Id)
        {
            var quote = abstractMasterBannerServices.MasterBanner_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterBanner_All")]
        public async Task<IHttpActionResult> MasterBanner_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterBannerServices.MasterBanner_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterBanner_Delete")]
        public async Task<IHttpActionResult> MasterBanner_Delete(int Id)
        {
            var quote = abstractMasterBannerServices.MasterBanner_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}