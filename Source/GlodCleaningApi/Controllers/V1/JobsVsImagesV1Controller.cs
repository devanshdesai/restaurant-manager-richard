﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using System.IO;

namespace GlodCleaningApi.Controllers.V1
{
    public class JobsVsImagesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobsVsImagesServices  abstractJobsVsImagesServices;

        #endregion

        #region Cnstr
        public JobsVsImagesV1Controller(AbstractJobsVsImagesServices abstractJobsVsImagesServices)
        {
            this.abstractJobsVsImagesServices = abstractJobsVsImagesServices;
        }
        #endregion

        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobsVsImages_Upsert")]
        public async Task<IHttpActionResult> JobsVsImages_Upsert()
        {
            JobsVsImages jobVsImages = new JobsVsImages();
            var httpRequest = HttpContext.Current.Request;
            jobVsImages.JobId = Convert.ToInt32(httpRequest.Params["JobId"]);
            jobVsImages.ImageCategoryName = Convert.ToString(httpRequest.Params["ImageCategoryName"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "JobImages/" + jobVsImages.JobId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                jobVsImages.Url = basePath + fileName;
            }

            var quote = abstractJobsVsImagesServices.JobsVsImages_Upsert(jobVsImages);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobsVsImages_ByJobId")]
        public async Task<IHttpActionResult> JobsVsImages_ByJobId(PageParam pageParam,string search="",int JobId=0)
        {
            var quote = abstractJobsVsImagesServices.JobsVsImages_ByJobId(pageParam,search,JobId);
            return this.Content((HttpStatusCode)200, quote);
        }
     
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobsVsImages_Delete")]
        public async Task<IHttpActionResult> JobsVsImages_Delete(int Id)
        {
            var quote = abstractJobsVsImagesServices.JobsVsImages_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        

    }
}
