﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using System.IO;

namespace GlodCleaningApi.Controllers.V1
{
    public class CleanerVsDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCleanerVsDocumentsServices abstractCleanerVsDocumentsServices;

        #endregion

        #region Cnstr
        public CleanerVsDocumentsV1Controller(AbstractCleanerVsDocumentsServices abstractCleanerVsDocumentsServices)
        {
            this.abstractCleanerVsDocumentsServices = abstractCleanerVsDocumentsServices;
        }
        #endregion

    
        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsDocuments_Delete")]
        public async Task<IHttpActionResult> CleanerVsDocuments_Delete(int Id)
        {
            var quote = abstractCleanerVsDocumentsServices.CleanerVsDocuments_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsDocuments_Upsert")]
        public async Task<IHttpActionResult> CleanerVsDocuments_Upsert()
        {
            CleanerVsDocuments carriers = new CleanerVsDocuments();

            var httpRequest = HttpContext.Current.Request;
            carriers.CleanerId = Convert.ToInt32(httpRequest.Params["CleanerId"]);
            carriers.DocumentType = Convert.ToInt32(httpRequest.Params["DocumentType"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "Documents/CleanerDocuments/" + carriers.CleanerId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                carriers.Url = basePath + fileName;
            }

            var quote = abstractCleanerVsDocumentsServices.CleanerVsDocuments_Upsert(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsDocuments_All")]
        public async Task<IHttpActionResult> CleanerVsDocuments_ByCleanerId(PageParam pageParam, int CleanerId,string search="")
        {
            var quote = abstractCleanerVsDocumentsServices.CleanerVsDocuments_ByCleanerId(pageParam, search, CleanerId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}