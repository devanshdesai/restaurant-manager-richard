﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterServiceCategoryV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterServiceCategoryServices abstractMasterServiceCategoryServices;

        #endregion

        #region Cnstr
        public MasterServiceCategoryV1Controller(AbstractMasterServiceCategoryServices abstractMasterServiceCategoryServices)
        {
            this.abstractMasterServiceCategoryServices = abstractMasterServiceCategoryServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceCategory_Upsert")]
        public async Task<IHttpActionResult> MasterServiceCategory_Upsert(MasterServiceCategory masterpromotionalcode)
        {
            var quote = abstractMasterServiceCategoryServices.MasterServiceCategory_Upsert(masterpromotionalcode);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceCategory_ById")]
        public async Task<IHttpActionResult> MasterServiceCategory_ById(int Id)
        {
            var quote = abstractMasterServiceCategoryServices.MasterServiceCategory_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceCategory_ByPromoCode")]
        public async Task<IHttpActionResult> MasterServiceCategory_ByParentId(int ParentId)
        {
            var quote = abstractMasterServiceCategoryServices.MasterServiceCategory_ByParentId(ParentId);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceCategory_All")]
        public async Task<IHttpActionResult> MasterServiceCategory_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterServiceCategoryServices.MasterServiceCategory_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceCategory_Delete")]
        public async Task<IHttpActionResult> MasterServiceCategory_Delete(int Id)
        {
            var quote = abstractMasterServiceCategoryServices.MasterServiceCategory_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterServiceCategory_ActInact")]
        public async Task<IHttpActionResult> MasterServiceCategory_ActInact(int Id)
        {
            var quote = abstractMasterServiceCategoryServices.MasterServiceCategory_ActInact(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}