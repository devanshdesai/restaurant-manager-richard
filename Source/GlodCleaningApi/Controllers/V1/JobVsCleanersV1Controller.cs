﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class JobVsCleanersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobVsCleanersServices  abstractJobVsCleanersServices;

        #endregion

        #region Cnstr
        public JobVsCleanersV1Controller(AbstractJobVsCleanersServices abstractJobVsCleanersServices)
        {
            this.abstractJobVsCleanersServices = abstractJobVsCleanersServices;
        }
        #endregion

        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsCleaners_Upsert")]
        public async Task<IHttpActionResult> JobVsCleaners_Upsert(JobVsCleaners jobVsCleaners)
        {
            var quote = abstractJobVsCleanersServices.JobVsCleaners_Upsert(jobVsCleaners);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsCleaners_All")]
        public async Task<IHttpActionResult> JobVsCleaners_ByJobId(PageParam pageParam,string search="",int JobId=0)
        {
            var quote = abstractJobVsCleanersServices.JobVsCleaners_ByJobId(pageParam,search,JobId);
            return this.Content((HttpStatusCode)200, quote);
        }
     
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsCleaners_ActInact")]
        public async Task<IHttpActionResult> JobVsCleaners_ActInact(int Id)
        {
            var quote = abstractJobVsCleanersServices.JobVsCleaners_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

    }
}
