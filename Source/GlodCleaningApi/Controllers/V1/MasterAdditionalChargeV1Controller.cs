﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterAdditionalChargeV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterAdditionalChargeServices abstractMasterAdditionalChargeServices;

        #endregion

        #region Cnstr
        public MasterAdditionalChargeV1Controller(AbstractMasterAdditionalChargeServices abstractMasterAdditionalChargeServices)
        {
            this.abstractMasterAdditionalChargeServices = abstractMasterAdditionalChargeServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterAdditionalCharge_All")]
        public async Task<IHttpActionResult> MasterAdditionalCharge_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterAdditionalChargeServices.MasterAdditionalCharge_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}