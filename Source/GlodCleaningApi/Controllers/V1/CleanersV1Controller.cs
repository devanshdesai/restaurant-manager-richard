﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using System.IO;

namespace GlodCleaningApi.Controllers.V1
{
    public class CleanersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCleanersServices abstractCleanersServices;

        #endregion

        #region Cnstr
        public CleanersV1Controller(AbstractCleanersServices abstractCleanersServices)
        {
            this.abstractCleanersServices = abstractCleanersServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_Login")]
        public async Task<IHttpActionResult> Cleaners_Login(Cleaners cleaners)
        {
            var quote = abstractCleanersServices.Cleaners_Login(cleaners);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_Logout")]
        public async Task<IHttpActionResult> Cleaners_Logout(int Id)
        {
            var quote = abstractCleanersServices.Cleaners_Logout(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_Upsert")]
        public async Task<IHttpActionResult> Cleaners_Upsert(Cleaners cleaners)
        {
            var quote = abstractCleanersServices.Cleaners_Upsert(cleaners);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_ById")]
        public async Task<IHttpActionResult> Cleaners_ById(int Id)
        {
            var quote = abstractCleanersServices.Cleaners_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_All")]
        public async Task<IHttpActionResult> Cleaners_All(PageParam pageParam, string search="")
        {
            var quote = abstractCleanersServices.Cleaners_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_ActInact")]
        public async Task<IHttpActionResult> Cleaners_ActInact(int Id)
        {
            var quote = abstractCleanersServices.Cleaners_ActInact(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_Approve")]
        public async Task<IHttpActionResult> Cleaners_AdminApproved(int Id)
        {
            var quote = abstractCleanersServices.Cleaners_AdminApproved(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_ChangePassword")]
        public async Task<IHttpActionResult> Cleaners_ChangePassword(Cleaners cleaners)
        {
            var quote = abstractCleanersServices.Cleaners_ChangePassword(cleaners);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Cleaners_ProfileUpdate")]
        public async Task<IHttpActionResult> Cleaners_ProfileUpdate()
        {
            Cleaners cleaners = new Cleaners();
            var httpRequest = HttpContext.Current.Request;
            cleaners.Id = Convert.ToInt32(httpRequest.Params["Id"]);

            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CleanerImages/" + cleaners.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                cleaners.AvatarPhotoCopy = basePath + fileName;
            }
            var quote = abstractCleanersServices.Cleaners_ProfileUpdate(cleaners);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
    }
}