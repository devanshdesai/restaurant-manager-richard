﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class JobVsExtrasV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobVsExtrasServices  abstractJobVsExtrasServices;

        #endregion

        #region Cnstr
        public JobVsExtrasV1Controller(AbstractJobVsExtrasServices abstractJobVsExtrasServices)
        {
            this.abstractJobVsExtrasServices = abstractJobVsExtrasServices;
        }
        #endregion

        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsExtras_Upsert")]
        public async Task<IHttpActionResult> JobVsExtras_Upsert(JobVsExtras jobVsExtraAmount)
        {
            var quote = abstractJobVsExtrasServices.JobVsExtras_Upsert(jobVsExtraAmount);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsExtras_All")]
        public async Task<IHttpActionResult> JobVsExtras_ByJobId(PageParam pageParam,string search="",int JobId=0)
        {
            var quote = abstractJobVsExtrasServices.JobVsExtras_ByJobId(pageParam,search,JobId);
            return this.Content((HttpStatusCode)200, quote);
        }
     
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobVsExtras_Delete")]
        public async Task<IHttpActionResult> JobVsExtras_Delete(int Id)
        {
            var quote = abstractJobVsExtrasServices.JobVsExtras_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        

    }
}
