﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class CleanerVsPrefferedWorkLocationsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCleanerVsPrefferedWorkLocationsServices abstractCleanerVsPrefferedWorkLocationsServices;

        #endregion

        #region Cnstr
        public CleanerVsPrefferedWorkLocationsV1Controller(AbstractCleanerVsPrefferedWorkLocationsServices abstractCleanerVsPrefferedWorkLocationsServices)
        {
            this.abstractCleanerVsPrefferedWorkLocationsServices = abstractCleanerVsPrefferedWorkLocationsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsPrefferedWorkLocations_Upsert")]
        public async Task<IHttpActionResult> CleanerVsPrefferedWorkLocations_Upsert(CleanerVsPrefferedWorkLocations carriers)
        {
            var quote = abstractCleanerVsPrefferedWorkLocationsServices.CleanerVsPrefferedWorkLocations_Upsert(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsPrefferedWorkLocations_ById")]
        public async Task<IHttpActionResult> CleanerVsPrefferedWorkLocations_ById(int Id)
        {
            var quote = abstractCleanerVsPrefferedWorkLocationsServices.CleanerVsPrefferedWorkLocations_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsPrefferedWorkLocations_ByCleanerId")]
        public async Task<IHttpActionResult> CleanerVsPrefferedWorkLocations_ByCleanerId(PageParam pageParam, int CleanerId,string search="")
        {
            var quote = abstractCleanerVsPrefferedWorkLocationsServices.CleanerVsPrefferedWorkLocations_ByCleanerId(pageParam, search , CleanerId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("CleanerVsPrefferedWorkLocations_Delete")]
        public async Task<IHttpActionResult> CleanerVsPrefferedWorkLocations_Delete(int Id, int CleanerId)
        {
            var quote = abstractCleanerVsPrefferedWorkLocationsServices.CleanerVsPrefferedWorkLocations_Delete(Id , CleanerId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}