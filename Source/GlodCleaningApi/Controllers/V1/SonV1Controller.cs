﻿using GlodCleaning.Common;
using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.V1;


namespace GlodCleaningApi.Controllers.V1
{
    public class SonV1Controller : AbstractBaseController
    {
        private readonly AbstractSonServices abstractSonServices;

        public SonV1Controller(AbstractSonServices abstractSonServices)
        {
            this.abstractSonServices=abstractSonServices;
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Son_Upsert")]
        public async Task<IHttpActionResult> Son_Upsert(AbstractSon abstractSon)
        {
            var quote = abstractSonServices.Son_Upsert(abstractSon);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Son_ById")]
        public async Task<IHttpActionResult> Son_ById(int Id)
        {
            var quote = abstractSonServices.Son_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Son_Delete")]
        public async Task<IHttpActionResult> Son_Delete(int Id)
        {
            var quote = abstractSonServices.Son_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("Son_All")]
        public async Task<IHttpActionResult> Son_All(PageParam pageParam, string Search, int MainMemberId, int BloodGroupId, int Gender, int Engaged)
        {
            var quote = abstractSonServices.Son_All(pageParam, Search, MainMemberId, BloodGroupId, Engaged, Gender);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}