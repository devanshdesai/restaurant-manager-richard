﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterJobStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterJobStatusServices abstractMasterJobStatusServices;

        #endregion

        #region Cnstr
        public MasterJobStatusV1Controller(AbstractMasterJobStatusServices abstractMasterJobStatusServices)
        {
            this.abstractMasterJobStatusServices = abstractMasterJobStatusServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterJobStatus_All")]
        public async Task<IHttpActionResult> MasterJobStatus_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterJobStatusServices.MasterJobStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}