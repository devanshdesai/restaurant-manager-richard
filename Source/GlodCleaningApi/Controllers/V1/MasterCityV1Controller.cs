﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterCityV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterCityServices abstractMasterCityServices;

        #endregion

        #region Cnstr
        public MasterCityV1Controller(AbstractMasterCityServices abstractMasterCityServices)
        {
            this.abstractMasterCityServices = abstractMasterCityServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterCity_ByStateId")]
        public async Task<IHttpActionResult> MasterCity_ByStateId(PageParam pageParam, int StateId, string search="")
        {
            var quote = abstractMasterCityServices.MasterCity_ByStateId(pageParam, search, StateId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}