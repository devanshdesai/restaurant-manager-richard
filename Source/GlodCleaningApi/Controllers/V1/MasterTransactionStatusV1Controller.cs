﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class MasterTransactionStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterTransactionStatusServices abstractMasterTransactionStatusServices;

        #endregion

        #region Cnstr
        public MasterTransactionStatusV1Controller(AbstractMasterTransactionStatusServices abstractMasterTransactionStatusServices)
        {
            this.abstractMasterTransactionStatusServices = abstractMasterTransactionStatusServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterTransactionStatus_All")]
        public async Task<IHttpActionResult> MasterTransactionStatus_All(PageParam pageParam, string search="")
        {
            var quote = abstractMasterTransactionStatusServices.MasterTransactionStatus_All(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}