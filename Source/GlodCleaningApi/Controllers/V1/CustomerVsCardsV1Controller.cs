﻿using GlodCleaning.APICommon;
using GlodCleaningApi.Models;
using GlodCleaning.Common;
using GlodCleaningApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using GlodCleaning.Common.Paging;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaningApi.Controllers.V1
{
    public class CustomerVsCardsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerVsCardsServices abstractCustomerVsCardsServices;

        #endregion

        #region Cnstr
        public CustomerVsCardsV1Controller(AbstractCustomerVsCardsServices abstractCustomerVsCardsServices)
        {
            this.abstractCustomerVsCardsServices = abstractCustomerVsCardsServices;
        }
        #endregion

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsCards_Upsert")]
        public async Task<IHttpActionResult> CustomerVsCards_Upsert(CustomerVsCards customersVsCards)
        {
            var quote = abstractCustomerVsCardsServices.CustomerVsCards_Upsert(customersVsCards);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsCards_ById")]
        public async Task<IHttpActionResult> CustomerVsCards_ById(int Id)
        {
            var quote = abstractCustomerVsCardsServices.CustomerVsCards_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsCards_All")]
        public async Task<IHttpActionResult> CustomerVsCards_ByCustomerId(PageParam pageParam,string search="",int CustomerId = 0)
        {
            var quote = abstractCustomerVsCardsServices.CustomerVsCards_ByCustomerId(pageParam,search,CustomerId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsCards_IsDefault")]
        public async Task<IHttpActionResult> CustomerVsCards_IsDefault(int Id)
        {
            var quote = abstractCustomerVsCardsServices.CustomerVsCards_IsDefault(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerVsCards_Delete")]
        public async Task<IHttpActionResult> CustomerVsCards_Delete(CustomerVsCards customersVsCards)
        {
            var quote = abstractCustomerVsCardsServices.CustomerVsCards_Delete(customersVsCards);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
