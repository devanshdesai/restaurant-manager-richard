﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using GlodCleaning.Infrastructure;
using GlodCleaning.Pages;
using GlodCleaning.Services.Contract;

namespace GlodCleaning.Controllers
{
    public class UserController : Controller
    {
        #region Fields
        private readonly AbstractUsersServices abstractUsersServices;
        private readonly AbstractRestaurantDataServices abstractRestaurantDataServices;

        #endregion

        #region Ctor
        public UserController(AbstractUsersServices abstractUsersServices, AbstractRestaurantDataServices abstractRestaurantDataServices)
        {
            this.abstractUsersServices = abstractUsersServices;
            this.abstractRestaurantDataServices = abstractRestaurantDataServices;

        }
        #endregion


        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);

                AbstractUsers abstractUsers = new Users();
                abstractUsers.UserType = 2;

                var model = abstractUsersServices.Users_All(pageParam, search, abstractUsers);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.RestaurantDetails)]
        public ActionResult RestaurantDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindRestaurantData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string ReservationDate = "",string ReservationTime="",int UserId=0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                string search = Convert.ToString(requestModel.Search.Value);

                AbstractRestaurantData abstractRestaurantData = new RestaurantData();

                HttpCookie reqCookie = Request.Cookies["UserLogin"];

                int UserType = Convert.ToInt32(Convert.ToString(reqCookie["UserType"]));
                
                if(UserType == 2)
                {
                    abstractRestaurantData.UserId = Convert.ToInt32(Convert.ToString(reqCookie["UserId"]));
                }
                else
                {
                    if(UserId > 0)
                    {
                        abstractRestaurantData.UserId = UserId;
                    }
                    else
                    {
                        abstractRestaurantData.UserId = 0;
                    }
                    
                }
                

                var model = abstractRestaurantDataServices.RestaurantData_All(pageParam, search, ReservationDate, ReservationTime, abstractRestaurantData.UserId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ActInact(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            abstractUsersServices.Users_ActInact(Id);
            return Json("Committee Member status updated successfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UsersDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));

            ViewBag.Id = Id;
            return View();
        }
        [HttpPost]
        public JsonResult GetById(int Id=0)
        {

            SuccessResult<AbstractUsers> successResult = abstractUsersServices.Users_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }


       
        [HttpPost]
        public JsonResult Upsert(Users users, HttpPostedFileBase uploadFile)
        {

            int Id = users.Id;

            if (uploadFile != null)
            {
                string basePath = "Users/" + users.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(uploadFile.FileName);
                string path = Server.MapPath("~/" + basePath);
                if (!Directory.Exists(Path.Combine(HttpContext.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Server.MapPath("~/" + basePath));
                }
                uploadFile.SaveAs(HttpContext.Server.MapPath("~/" + basePath + fileName));
                users.Logo = basePath + fileName;

            }

            SuccessResult<AbstractUsers> result = abstractUsersServices.Users_Upsert(users);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion


    }
}