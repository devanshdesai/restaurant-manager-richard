﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using GlodCleaning.Infrastructure;
using GlodCleaning.Pages;
using GlodCleaning.Services.Contract;

namespace GlodCleaning.Controllers
{
    public class ProfileController : BaseController
    {
        #region Fields
        private readonly AbstractUsersServices abstractUsersServices;
        #endregion

        #region Ctor
        public ProfileController(AbstractUsersServices abstractUsersServices)
        {
            this.abstractUsersServices = abstractUsersServices;
        }
        #endregion

        #region Methods

        public ActionResult ChangePassword()
        {
            return View();
        }


        [HttpPost]
        public JsonResult Upsert(string oldPassword, string newPassword, string confirmPassword)
        {
            SuccessResult<AbstractUsers> result = abstractUsersServices.Users_ById(ProjectSession.UserID);
            if (result.Item.Password != oldPassword)
            {
                return Json("400", JsonRequestBehavior.AllowGet);
            }
            if (newPassword != confirmPassword)
            {
                return Json("403", JsonRequestBehavior.AllowGet);
            }

            Users tdd_Client = new Users();
            tdd_Client.Id = ProjectSession.UserID;
            tdd_Client.OldPassword = oldPassword;
            tdd_Client.NewPassword = newPassword;
            tdd_Client.ConfirmPassword = confirmPassword;
            abstractUsersServices.Users_ChangePassword(tdd_Client);
            return Json("200", JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}