﻿using System.Web.Mvc;
using GlodCleaning.Infrastructure;
using GlodCleaning.Pages;

namespace GlodCleaning.Controllers
{
    public class DashboardController : BaseController
    {
        #region Fields
        #endregion

        #region Ctor
        #endregion

        #region Methods

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            return View();
        }
        #endregion
    }
}