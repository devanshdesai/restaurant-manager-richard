﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GlodCleaning.Common;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using GlodCleaning.Infrastructure;
using GlodCleaning.Pages;
using GlodCleaning.Services.Contract;


namespace GlodCleaning.Controllers
{
    public class AuthenticationController : Controller
    {
        #region Fields
        private readonly AbstractUsersServices abstractUsersServices;
        #endregion


        #region Constructor

        public AuthenticationController(AbstractUsersServices abstractUsersServices)
        {
            this.abstractUsersServices = abstractUsersServices;
        }

        #endregion

        // GET: Home
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Login(string email, string password)
        {

            AbstractUsers users = new Users();
            users.Email = email;
            users.Password = password;
            SuccessResult<AbstractUsers> result = abstractUsersServices.Users_LogIn(users);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                Session.Clear();

                ProjectSession.UserId = result.Item.Id;
                ProjectSession.UserType = result.Item.UserType;
                ProjectSession.UserName = result.Item.Name;
                ProjectSession.Email = result.Item.Email;


                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("UserId", Convert.ToString(result.Item.Id));
                cookie.Values.Add("UserType", ProjectSession.UserType.ToString());
                cookie.Values.Add("AdminRoleId", Convert.ToString(result.Item.UserType));
                cookie.Values.Add("AdminUserName", result.Item.Name);
                cookie.Expires = DateTime.Now.AddDays(365);
                Response.Cookies.Add(cookie);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Session.Clear();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //[ActionName(Actions.Logout)]
        //public ActionResult Logout()
        //{


        //    if (Request.Cookies["AdminUserLogin"] != null)
        //    {


        //        abstractUsersServices.Users_LogOut(ProjectSession.UserId);

        //        var c = new HttpCookie("AdminUserLogin");
        //        c.Expires = DateTime.Now.AddDays(-1);
        //        Response.Cookies.Add(c);
        //    }
        //    Session.Clear();
        //    Session.Abandon();

        //    Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Cache.SetNoStore();

        //    return RedirectToAction(Actions.Login, GlodCleaning.Pages.Controllers.Authentication);
        //}

        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            int UserType = 0;
            UserType = ProjectSession.UserType;

            if (Request.Cookies["UserLogin"] != null)
            {
                abstractUsersServices.Users_LogOut(ProjectSession.UserId);

                string[] myCookies = Request.Cookies.AllKeys;
                foreach (string cookie in myCookies)
                {
                    Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                }
            }
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();


            if (UserType == 1 || UserType == 2)
            {
                return RedirectToAction(Actions.Login, Pages.Controllers.Authentication);
            }
            else
            {
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);
            }
        }


        //[ActionName(Actions.Logout)]
        //public ActionResult Logout()
        //{
        //    int UserType = 0;
        //    UserType = ProjectSession.UserType;

        //    if (Request.Cookies["UsersLogin"] != null)
        //    {
        //        abstractUsersServices.Users_LogOut(ProjectSession.UserId);

        //        var c = new HttpCookie("UsersLogin");
        //        c.Expires = DateTime.Now.AddDays(-1);
        //        Response.Cookies.Add(c);
        //    }
        //    Session.Clear();
        //    Session.Abandon();

        //    Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Cache.SetNoStore();
        //        return RedirectToAction(Actions.Login, Pages.Controllers.Authentication);


        //}



    }
}