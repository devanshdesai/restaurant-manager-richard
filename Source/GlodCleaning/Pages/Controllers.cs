﻿namespace GlodCleaning.Pages
{
    public class Controllers
    {
        public const string User = "User";
        public const string Role = "Role";
        public const string Dashboard = "Dashboard";
        public const string Authentication = "Authentication";
        public const string Authentication1 = "Authentication1";
        public const string Profile = "Profile";
        public const string Customers = "Customers";
        public const string Cleaners = "Cleaners";
        public const string Providers = "Providers";
        public const string MasterPromotionalCode = "MasterPromotionalCode";
        public const string Support= "Support";      
        public const string Jobs = "Jobs";
        public const string Donors = "Donors";
        public const string LatestAnnouncement = "LatestAnnouncement";
        public const string LatestEvent = "LatestEvent";
        public const string RitRivaz = "RitRivaz";
        public const string Wife = "Wife";
        public const string MainMember = "MainMember";
        public const string CommitteeMember = "CommitteeMember";
        public const string PhotoGallery = "PhotoGallery";
        public const string Daughter = "Daughter";
        public const string SonChild = "SonChild";
        public const string SonWife = "SonWife";
        public const string VideoGallery = "VideoGallery";
        public const string Son = "Son";
        public const string PageImages = "PageImages";
        public const string Advertisement = "Advertisement";
        public const string RestauranForm = "RestauranForm";
        public const string Message = "Message";
        public const string Home = "Home";
    }
}