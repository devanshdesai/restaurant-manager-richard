﻿namespace GlodCleaning.Pages
{
    public class Actions
    {
        #region Common
        public const string Index = "Index";
        public const string BindData = "BindData";
        public const string Upsert = "Upsert";
        public const string ActInact = "ActInact";
        public const string ApprovedCleaner = "ApprovedCleaner";
        public const string ApprovedProvider = "ApprovedProvider";
        public const string GetById ="GetById";
        public const string CustomerDetails ="CustomerDetails";
        public const string CleanerDetails ="CleanerDetails";
        public const string ProviderDetails = "ProviderDetails";
        public const string PromotionalCodeDetails ="PromotionalCodeDetails";
        public const string DeletePromotionalCode = "DeletePromotionalCode";
        public const string SupportDetails ="SupportDetails";
        public const string CommitteeMemberDetails = "CommitteeMemberDetails";
        public const string DeleteCommitteeMember ="DeleteCommitteeMember";
        public const string ImageUpsert ="ImageUpsert";
        public const string DonorsDetails = "DonorsDetails";
        public const string DeleteDonors = "DeleteDonors";
        public const string MainMemberDetails = "MainMemberDetails";
        public const string DeleteMainMember = "DeleteMainMember";
        public const string LatestAnnouncementDetails = "LatestAnnouncementDetails";
        public const string DeleteLatestAnnouncement = "DeleteLatestAnnouncement";
        public const string LatestEventDetails = "LatestEventDetails";
        public const string DeleteLatestEvent = "DeleteLatestEvent";
        public const string RitRivazDetails = "RitRivazDetails";
        public const string WifeDetails= "WifeDetails";
        public const string DeleteWife = "DeleteWife";
        public const string PhotoUpload = "PhotoUpload";
        public const string DaughterDetails = "DaughterDetails";
        public const string DeleteDaughter = "DeleteDaughter";
        public const string SonWifeDetails = "SonWifeDetails";
        public const string DeleteSonWife = "DeleteSonWife";
        public const string SonChildDetails = "SonChildDetails";
        public const string DeleteSonChild = "DeleteSonChild";
        public const string VideoGalleryDetails = "VideoGalleryDetails";
        public const string DeleteVideoGallery = "DeleteVideoGallery";
        public const string DeletePhotoGallery = "DeletePhotoGallery";
        public const string DeleteRitRivaz = "DeleteRitRivaz";
        public const string DeleteSon = "DeleteSon";
        public const string SonDetails = "SonDetails";
        public const string BindSonData = "BindSonData";
        public const string BindWifeData = "BindWifeData";
        public const string BindDaughterData= "BindDaughterData";
        public const string BindSonChildData = "BindSonChildData";
        public const string BindSonWifeData = "BindSonWifeData";
        public const string DeletePageImages = "DeletePageImages";
        public const string DeleteAdvertisement = "DeleteAdvertisement";
        #endregion

        #region Authentication
        public const string Login = "Login";
        public const string SignIn = "SignIn";
        public const string Logout = "Logout";


        #endregion
        #region Restaurant
        public const string RestaurantDetails = "RestaurantDetails";
        public const string BindRestaurantData = "BindRestaurantData";
        #endregion

        #region Profile
        public const string ChangePassword = "ChangePassword";
        #endregion

        #region RestForm
        public const string Success = "Success";
        #endregion

    }
}