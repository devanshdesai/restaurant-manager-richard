﻿using GlodCleaning.Common;
using GlodCleaning.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;

namespace GlodCleaning.Infrastructure
{
    public class BaseController : Controller
    {
        #region Fields
        public static string AdminUserName;
        public static string CheckType;
        public static string CheckLastDate;
        #endregion

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }
        private bool IsAllowed(string controllerName, string[] accessControllers)
        {
            foreach (string accessController in accessControllers)
            {
                if (accessController.ToLower() == controllerName)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                bool isAllow = false;

                if (ProjectSession.UserId == null || ProjectSession.UserId == 0)
                {
                    filterContext.Result = new RedirectResult("~/Authentication/Login");
                    return;
                }

                //if(ProjectSession.UserTypeId==(int)UserTypeEnum.Admin&&
                //    IsAllowed(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                //    Pages.Controllers.AdminAccess
                //    ))
                //{
                //    isAllow=true;
                //}

                //if(ProjectSession.UserTypeId==(int)UserTypeEnum.UnderWriter&&
                //    IsAllowed(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                //    Pages.Controllers.UnderWriterAccess
                //    ))
                //{
                //    isAllow=true;
                //}

                //if(ProjectSession.UserTypeId==(int)UserTypeEnum.Dealer&&
                //    IsAllowed(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                //    Pages.Controllers.DealerAccess
                //    ))
                //{
                //    isAllow=true;
                //}

                //if(ProjectSession.UserTypeId==(int)UserTypeEnum.Seller&&
                //  IsAllowed(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                //  Pages.Controllers.SellerAccess
                //  ))
                //{
                //    isAllow=true;
                //}


                //if(!isAllow)
                //{
                //    filterContext.Result=new RedirectResult("~/Authentication/Logout");
                //    return;
                //}
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                throw ex;
            }
        }

    }
}