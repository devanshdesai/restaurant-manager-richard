﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GlodCleaning.Services
{
    using Autofac; 
    
    using GlodCleaning.Services.Contract;

    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {           
            builder.RegisterModule<DataModule>();
            builder.RegisterType<V1.AdminServices>().As<AbstractAdminServices>().InstancePerDependency();
            builder.RegisterType<V1.Admin1Services>().As<AbstractAdmin1Services>().InstancePerDependency();
            builder.RegisterType<V1.TransactionServices>().As<AbstractTransactionServices>().InstancePerDependency();
            builder.RegisterType<V1.UsersServices>().As<AbstractUsersServices>().InstancePerDependency();
            builder.RegisterType<V1.QuoteServices>().As<AbstractQuoteServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomersServices>().As<AbstractCustomersServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerVsCardsServices>().As<AbstractCustomerVsCardsServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerVsCardsServices>().As<AbstractCustomerVsCardsServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerVsAddressServices>().As<AbstractCustomerVsAddressServices>().InstancePerDependency();
            builder.RegisterType<V1.JobVsCleanersServices>().As<AbstractJobVsCleanersServices>().InstancePerDependency();
            builder.RegisterType<V1.JobVsExtraAmountServices>().As<AbstractJobVsExtraAmountServices>().InstancePerDependency();
            builder.RegisterType<V1.JobVsExtrasServices>().As<AbstractJobVsExtrasServices>().InstancePerDependency();
            builder.RegisterType<V1.JobVsReviewServices>().As<AbstractJobVsReviewServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterExtraServices>().As<AbstractMasterExtraServices>().InstancePerDependency();
            builder.RegisterType<V1.JobServices>().As<AbstractJobServices>().InstancePerDependency();
            builder.RegisterType<V1.CleanersServices>().As<AbstractCleanersServices>().InstancePerDependency();
            builder.RegisterType<V1.CleanerVsDetailsServices>().As<AbstractCleanerVsDetailsServices>().InstancePerDependency();
            builder.RegisterType<V1.CleanerVsDocumentsServices>().As<AbstractCleanerVsDocumentsServices>().InstancePerDependency();
            builder.RegisterType<V1.CleanerVsPrefferedWorkLocationsServices>().As<AbstractCleanerVsPrefferedWorkLocationsServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterAdditionalChargeServices>().As<AbstractMasterAdditionalChargeServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterAddressTypeServices>().As<AbstractMasterAddressTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterBannerServices>().As<AbstractMasterBannerServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterCityServices>().As<AbstractMasterCityServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterCountryServices>().As<AbstractMasterCountryServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterDocumentTypeServices>().As<AbstractMasterDocumentTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterServiceCategoryServices>().As<AbstractMasterServiceCategoryServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterServiceTypesServices>().As<AbstractMasterServiceTypesServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterStateServices>().As<AbstractMasterStateServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterSuburbServices>().As<AbstractMasterSuburbServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterTransactionStatusServices>().As<AbstractMasterTransactionStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterJobStatusServices>().As<AbstractMasterJobStatusServices>().InstancePerDependency();
            builder.RegisterType<V1.JobsVsImagesServices>().As<AbstractJobsVsImagesServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterPromotionalCodeServices>().As<AbstractMasterPromotionalCodeServices>().InstancePerDependency();
            builder.RegisterType<V1.SupportServices>().As<AbstractSupportServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterUserTypeServices>().As<AbstractMasterUserTypeServices>().InstancePerDependency();
            builder.RegisterType<V1.ProvidersServices>().As<AbstractProvidersServices>().InstancePerDependency();
            base.Load(builder);
        }
    }
}
