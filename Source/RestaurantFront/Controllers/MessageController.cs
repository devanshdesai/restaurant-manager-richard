﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using GlodCleaning.Common.Paging;

namespace RestaurantFront.Controllers
{
 
    public class MessageController : Controller
    {

        //Success Msg
        public ActionResult Success(string ri, string cname="")
        {
            ViewBag.Rid = ri;
            ViewBag.name = cname;
            return View();
        }

        // Wrong Msg
        public ActionResult Wrong(string ri)
        {
            ViewBag.Rid = ri;
            return View();
        }

    }
}