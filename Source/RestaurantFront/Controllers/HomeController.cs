﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GlodCleaning.Services.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;
using GlodCleaning.Common.Paging;
using GlodCleaning.Common;

namespace RestaurantFront.Controllers
{

    public class HomeController : Controller
    {

        #region Fields
        private readonly AbstractRestaurantDataServices abstractRestaurantDataServices = null;
        private readonly AbstractUsersServices abstractUsersServices = null;

        #endregion

        #region Ctor
        public HomeController(AbstractRestaurantDataServices abstractRestaurantDataServices , AbstractUsersServices abstractUsersServices)
        {
            this.abstractRestaurantDataServices = abstractRestaurantDataServices;
            this.abstractUsersServices = abstractUsersServices;

        }
        #endregion

        public ActionResult Index(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            ViewBag.RId = ri;
            var result = abstractUsersServices.Users_ById(Id);
            if(result.Item != null)
            {
                ViewBag.RestaurantName = result.Item.RestaurantName;
                ViewBag.Description = result.Item.Description;
                return View();
            }
            else
            {
                return RedirectToAction("Wrong","Message");
            }
        }

        [HttpPost]
        public JsonResult GetById(int Id)
        {
            // int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            SuccessResult<AbstractUsers> successResult = abstractUsersServices.Users_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult Upsert(int UserId, string NameOfCustomer, string TelephoneNumber, string ReservationDate , string HealthSymptoms, bool IsHouseholdFever, bool IsCovid, bool IsHouseholdCovid, bool IsQuarntine, string ReservationTime = "", int NoOfPeople = 0)
        {
            AbstractRestaurantData abstractRestaurantData = new RestaurantData();

            abstractRestaurantData.UserId = UserId;
            abstractRestaurantData.NameOfCustomer = NameOfCustomer;
            abstractRestaurantData.TelephoneNumber = TelephoneNumber;
            abstractRestaurantData.ReservationDate = ReservationDate;
            abstractRestaurantData.ReservationTime = ReservationTime;
            abstractRestaurantData.NoOfPeople = NoOfPeople;
            abstractRestaurantData.HealthSymptoms = HealthSymptoms;
            abstractRestaurantData.IsHouseholdFever = IsHouseholdFever;
            abstractRestaurantData.IsCovid = IsCovid;
            abstractRestaurantData.IsHouseholdCovid = IsHouseholdCovid;
            abstractRestaurantData.IsQuarntine = IsQuarntine;

            SuccessResult<AbstractRestaurantData> result = abstractRestaurantDataServices.RestaurantData_Upsert(abstractRestaurantData);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}