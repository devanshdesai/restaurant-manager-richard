﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;

namespace GlodCleaning.Data.Contract
{
   public abstract class AbstractUsersDao
    {
        public abstract SuccessResult<AbstractUsers> Users_ById(int Id);
        public abstract SuccessResult<AbstractUsers> Users_ActInact(int Id);
        public abstract SuccessResult<AbstractUsers> Users_UpdateLogo(int Id, string Logo);
        public abstract PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, AbstractUsers abstractUsers);
        public abstract SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers);
        public abstract SuccessResult<AbstractUsers> Users_LogIn(AbstractUsers abstractUsers);
        public abstract bool Users_LogOut(int Id);
        public abstract SuccessResult<AbstractUsers> Users_ChangePassword(AbstractUsers abstractUsers);
    }
}
