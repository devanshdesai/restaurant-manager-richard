﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Data.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaning.Data.V1
{
    public class RestaurantDataDao : AbstractRestaurantDataDao
    {

        public override SuccessResult<AbstractRestaurantData> RestaurantData_ById(int Id)
        {
            SuccessResult<AbstractRestaurantData> RestaurantData = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RestaurantData_ById, param, commandType: CommandType.StoredProcedure);
                RestaurantData = task.Read<SuccessResult<AbstractRestaurantData>>().SingleOrDefault();
                RestaurantData.Item = task.Read<RestaurantData>().SingleOrDefault();
            }

            return RestaurantData;
        }
        public override PagedList<AbstractRestaurantData> RestaurantData_All(PageParam pageParam, string search, string ReservationDate, string ReservationTime, int UserId)
        {
            PagedList<AbstractRestaurantData> RestaurantData = new PagedList<AbstractRestaurantData>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ReservationDate", ReservationDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ReservationTime", ReservationTime, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RestaurantData_All, param, commandType: CommandType.StoredProcedure);
                RestaurantData.Values.AddRange(task.Read<RestaurantData>());
                RestaurantData.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return RestaurantData;
        }
        
        public override SuccessResult<AbstractRestaurantData>RestaurantData_Upsert(AbstractRestaurantData abstractRestaurantData)
        {
            SuccessResult<AbstractRestaurantData> RestaurantData = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractRestaurantData.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractRestaurantData.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@NameOfCustomer", abstractRestaurantData.NameOfCustomer, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TelephoneNumber", abstractRestaurantData.TelephoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ReservationDate", abstractRestaurantData.ReservationDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ReservationTime", abstractRestaurantData.ReservationTime, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NoOfPeople", abstractRestaurantData.NoOfPeople, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@HealthSymptoms", abstractRestaurantData.HealthSymptoms, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsHouseholdFever", abstractRestaurantData.IsHouseholdFever, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsCovid", abstractRestaurantData.IsCovid, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsHouseholdCovid", abstractRestaurantData.IsHouseholdCovid, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsQuarntine", abstractRestaurantData.IsQuarntine, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Email", abstractRestaurantData.Email, dbType: DbType.String, direction: ParameterDirection.Input);




            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RestaurantData_Upsert, param, commandType: CommandType.StoredProcedure);
                RestaurantData = task.Read<SuccessResult<AbstractRestaurantData>>().SingleOrDefault();
                RestaurantData.Item = task.Read<RestaurantData>().SingleOrDefault();
            }

            return RestaurantData;
        }

    }

}
