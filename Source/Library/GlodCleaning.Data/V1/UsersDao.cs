﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Data.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Entities.V1;

namespace GlodCleaning.Data.V1
{
    public class UsersDao : AbstractUsersDao
    {

        public override SuccessResult<AbstractUsers> Users_ById(int Id)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ById, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string search, AbstractUsers abstractUsers)
        {
            PagedList<AbstractUsers> Users = new PagedList<AbstractUsers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractUsers.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_All, param, commandType: CommandType.StoredProcedure);
                Users.Values.AddRange(task.Read<Users>());
                Users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_ActInact(int Id)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ActInact, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_UpdateLogo(int Id , String Logo)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Logo", Id, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ActInact, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers>Users_Upsert(AbstractUsers abstractUsers)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUsers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractUsers.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractUsers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RestaurantName", abstractUsers.RestaurantName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractUsers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUsers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine1", abstractUsers.AddressLine1, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractUsers.AddressLine2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Lat", abstractUsers.Lat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Long", abstractUsers.Long, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractUsers.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Logo", abstractUsers.Logo, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractUsers.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_Upsert, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }

            return Users;
        }
        public override SuccessResult<AbstractUsers> Users_LogIn(AbstractUsers abstractUsers)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Email", abstractUsers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUsers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractUsers.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_LogIn, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }
            return Users;
        }

        public override SuccessResult<AbstractUsers> Users_ChangePassword(AbstractUsers abstractUsers)
        {
            SuccessResult<AbstractUsers> Users = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUsers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractUsers.OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractUsers.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", abstractUsers.ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Users_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                Users.Item = task.Read<Users>().SingleOrDefault();
            }
            return Users;
        }

        public override bool Users_LogOut(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Users_LogOut, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

    }

}
