﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GlodCleaning.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
          

        #region Users
        public const string Users_Upsert = "Users_Upsert";
        public const string Users_LogIn = "Users_LogIn";
        public const string Users_ActInact = "Users_ActInact";
        public const string Users_LogOut = "Users_LogOut";
        public const string Users_All = "Users_All";
        public const string Users_ById = "Users_ById";
        public const string Users_UpdateLogo = "Users_UpdateLogo";
        public const string Users_ChangePassword = "Users_ChangePassword";
        #endregion

        #region Users
        public const string RestaurantData_ById = "RestaurantData_ById";
        public const string RestaurantData_Upsert = "RestaurantData_Upsert";
        public const string RestaurantData_All = "RestaurantData_All";
        #endregion
    }
}
