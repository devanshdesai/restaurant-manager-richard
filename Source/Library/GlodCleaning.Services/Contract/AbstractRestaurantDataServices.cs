﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Entities.Contract;

namespace GlodCleaning.Services.Contract
{
   public abstract class AbstractRestaurantDataServices
    {
        public abstract SuccessResult<AbstractRestaurantData> RestaurantData_ById(int Id);
        public abstract PagedList<AbstractRestaurantData> RestaurantData_All(PageParam pageParam, string search, string ReservationDate, string ReservationTime,int UserId);
        public abstract SuccessResult<AbstractRestaurantData> RestaurantData_Upsert(AbstractRestaurantData abstractRestaurantData);
    }
}
