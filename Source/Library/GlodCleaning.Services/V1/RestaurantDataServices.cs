﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Data.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Services.Contract;

namespace GlodCleaning.Services.V1
{
    public class RestaurantDataServices : AbstractRestaurantDataServices
    {

        private AbstractRestaurantDataDao abstractRestaurantDataDao;
        public RestaurantDataServices(AbstractRestaurantDataDao abstractRestaurantDataDao)
        {
            this.abstractRestaurantDataDao = abstractRestaurantDataDao;
        }
        public override SuccessResult<AbstractRestaurantData> RestaurantData_Upsert(AbstractRestaurantData abstractRestaurantData)
        {
            return this.abstractRestaurantDataDao.RestaurantData_Upsert(abstractRestaurantData);
        }
        public override PagedList<AbstractRestaurantData> RestaurantData_All(PageParam pageParam, string Search, string ReservationDate, string ReservationTime,int UserId)
        {
            return this.abstractRestaurantDataDao.RestaurantData_All(pageParam, Search, ReservationDate, ReservationTime,UserId);
        }
        public override SuccessResult<AbstractRestaurantData> RestaurantData_ById(int Id)
        {
            return this.abstractRestaurantDataDao.RestaurantData_ById(Id);
        }
    }
}
