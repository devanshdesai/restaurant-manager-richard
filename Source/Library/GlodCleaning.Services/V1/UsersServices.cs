﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlodCleaning.Common;
using GlodCleaning.Common.Paging;
using GlodCleaning.Data.Contract;
using GlodCleaning.Entities.Contract;
using GlodCleaning.Services.Contract;

namespace GlodCleaning.Services.V1
{
    public class UsersServices : AbstractUsersServices
    {

        private AbstractUsersDao abstractUsersDao;
        public UsersServices(AbstractUsersDao abstractUsersDao)
        {
            this.abstractUsersDao = abstractUsersDao;
        }
        public override SuccessResult<AbstractUsers> Users_Upsert(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_Upsert(abstractUsers);
        }
        public override SuccessResult<AbstractUsers> Users_ChangePassword(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_ChangePassword(abstractUsers);
        }
        public override SuccessResult<AbstractUsers> Users_LogIn(AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_LogIn(abstractUsers);
        }
        public override SuccessResult<AbstractUsers> Users_ActInact(int Id)
        {
            return this.abstractUsersDao.Users_ActInact(Id);
        }
        public override PagedList<AbstractUsers> Users_All(PageParam pageParam, string Search, AbstractUsers abstractUsers)
        {
            return this.abstractUsersDao.Users_All(pageParam, Search, abstractUsers);
        }
        public override SuccessResult<AbstractUsers> Users_ById(int Id)
        {
            return this.abstractUsersDao.Users_ById(Id);
        }
        public override SuccessResult<AbstractUsers> Users_UpdateLogo(int Id ,string Logo)
        {
            return this.abstractUsersDao.Users_UpdateLogo(Id ,Logo);
        }
        public override bool Users_LogOut(int Id)
        {
            return this.abstractUsersDao.Users_LogOut(Id);
        }
    }
}
