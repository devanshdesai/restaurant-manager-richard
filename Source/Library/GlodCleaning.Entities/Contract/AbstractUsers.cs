﻿using GlodCleaning.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlodCleaning.Entities.Contract
{
    public abstract class AbstractUsers
    {
        public int Id { get; set; }
        public int UserType { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string RestaurantName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Description { get; set; }
        public string UserTypeName { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy") : "-";
        
        [NotMapped]
        public string LogoUrlStr => Configurations.ClientUrl + Logo;

    }
}
