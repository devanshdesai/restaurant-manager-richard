﻿using GlodCleaning.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlodCleaning.Entities.Contract
{
    public abstract class AbstractRestaurantData
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string NameOfCustomer { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public string ReservationDate { get; set; }
        public string ReservationTime { get; set; }
        public int NoOfPeople { get; set; }
        public string HealthSymptoms { get; set; }
        public bool IsHouseholdFever { get; set; }
        public bool IsCovid { get; set; }
        public bool IsHouseholdCovid { get; set; }
        public bool IsQuarntine { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm") : "-";

    }
}
